/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    API_URL: "https://staging-api.operational-livera.digitalnative.id/",
    BASE_URL: "http://localhost:3000/",
    // BASE_URL: "http://localhost:3000/"
    //API_URL: "https://api.operational-livera.digitalnative.id/",
        
  },
}

module.exports = nextConfig