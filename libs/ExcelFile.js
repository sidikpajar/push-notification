import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import api from 'api';
import React from 'react';

export const ExportToCSV = () => {
      api.post(`api/v1/admin/report/excel_order`)
      .then((response) => {
          const excel = response.data
          
          const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          const ws = XLSX.utils.json_to_sheet(excel); 
          const wb = {Sheets:{data:ws}, SheetNames:["data"]};
          const excelBuffer = XLSX.write(wb, {bookType:"xlsx", type:"array"});
          const data = new Blob([excelBuffer], {type:fileType});
          FileSaver.saveAs(data, "Data Transaksi"+".xlsx")
      })
      return(
        <button onClick={ExportToCSV} style={{ width: "100px", height: "40px", marginTop: "10px", backgroundColor: "#9FB866", color: "white", cursor: "pointer" }}>Export</button>
    )
  }