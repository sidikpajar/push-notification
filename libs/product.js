import api from "api";
import Router from "next/router";

//for first checking in dashboard, should change to session checker
export const checkProduct = async () => {
  api
    .get(`api/v1/admin/product`)
    .then((response) => {
      if (response.statusCode == 200) {
        Router.push("/");
      } else {
        api.defaults.headers.common["Authorization"] = null;
        Router.push(`/login`);
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const addProduct = async () => {
  /*
    let formData = new FormData();
    formData.append('productImage', productImage);
    formData.append('nama_product', nama_product);
    formData.append('harga_product', harga_product);
    formData.append('jenis_product', jenis_product);
    formData.append('isi_product', isi_product);

    axios.post(`${process.env.API_URL}api/v1/admin/add_product`, formData, {
        headers: {
            "Content-Type": "multipart/form-data"
        }
    })
    .then(() => {
        //console.log(response);
        console.log("Successfully added product");
    }).catch(error => {
        console.log(error);
    });
    */
};

export const editProduct = async () => {
  /*
    let formData = new FormData();
    formData.append('productImage', productImage);
    formData.append('nama_product', nama_product);
    formData.append('harga_product', harga_product);
    formData.append('jenis_product', jenis_product);
    formData.append('isi_product', isi_product);

    axios.put(`${process.env.API_URL}api/v1/admin/product/${productID}`, formData, {
        headers: {
            "Content-Type": "multipart/form-data"
        }
    })
    .then(() => {
        //console.log(response);
        console.log("Successfully updated product");
    }).catch(error => {
        console.log(error);
    });
    */
};

export const deleteProduct = async (productID) => {
  api
    .delete(`api/v1/admin/product/${productID}`)
    .then(() => {
      //console.log(response);
      console.log("Successfully deleted product");
    })
    .catch((error) => {
      console.log(error);
    });
};
