import moment from "moment";

const Shorting = (data, key, type_data)=>{
	if(type_data == 'string'){
		return data?.sort((a, b) => a[key].toLowerCase() < b[key].toLowerCase() && 1 || -1)
	}else if(type_data == 'number'){
		return data.sort((a,b)=> a[key] > b[key] && 1 || -1);
	}else if(type_data == 'date'){
		return data?.sort((a,b)=> moment(a[key]).format('YYYY-MM-DD') < moment(b[key]).format('YYYY-MM-DD') && 1 || -1);
	}
}

export default Shorting
