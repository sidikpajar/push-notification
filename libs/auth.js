import Router from "next/router";
import Swal from 'sweetalert2'
import api from "api";

export const loginUser = async (email, password) => {
  api
    .post(`api/v1/admin/login`, {
      email: email,
      password: password,
    })
    .then((response) => {
      //console.log(response);
      const data = response.data;
      if (data.statusCode == 200) {
        window.localStorage.setItem("LOGIN_TOKEN", data.data.token);
        api.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${data.data.token}`;
        Swal.fire({
          icon: 'success',
          title: 'Login Successfully!',
          showConfirmButton: false,
          timer: 1500,
          willClose: ()=> Router.replace("/")
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Login failed!',
          footer: 'Why do I have this issue?'
        })
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const logoutUser = async () => {
  api
    .post(`api/v1/admin/logout`)
    .then((response) => {
      //console.log(response);
      const data = response.data;
      if (data.statusCode == 200) {
        window.localStorage.setItem("LOGIN_TOKEN", null);
        api.defaults.headers.common["Authorization"] = null;
        Router.replace("/login")
      } else {
        Router.replace("/login")
      }
    })
    .catch((error) => {
      console.log(error);
    });
};
