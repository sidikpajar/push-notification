import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

export const ExportToExcel = ({dataProduk}) => {
    const fileType = "xlsx"
    const exportToCSV = () => {
        // console.log(dataTransaksi)
          const ws = XLSX.utils.json_to_sheet(dataProduk);
          const wb = {Sheets:{data:ws}, SheetNames:["data"]};
          const excelBuffer = XLSX.write(wb, {bookType:"xlsx", type:"array"});
          const data = new Blob([excelBuffer], {type:fileType});
          FileSaver.saveAs(data, "Data Product"+".xlsx")
    }

    return(
        <button onClick={exportToCSV} style={{ width: "100px", height: "40px", marginTop: "10px", backgroundColor: "#9FB866", color: "white", cursor: "pointer" }}>Export</button>
    )
}