import api from "api";

export const addOrder = async (
  nama_user,
  no_handphone,
  jmlh_pengiriman,
  tgl_pengiriman,
  deliveryID,
  metode_pengiriman,
  tipe_order,
  products,
  listProductPackage,
  note
) => {
  api
    .post(`api/v1/admin/order`, {
      nama_user: nama_user,
      no_handphone: no_handphone,
      jmlh_pengiriman: jmlh_pengiriman,
      tgl_pengiriman: tgl_pengiriman,
      deliveryID: deliveryID,
      metode_pengiriman: metode_pengiriman,
      tipe_order: tipe_order,
      products: products,
      listProductPackage: listProductPackage,
      note: note,
    })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
    });
};
