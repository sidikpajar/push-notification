import { ButtonBack, InputField } from "components";
import Head from "next/head";
import React from "react";
import OrderPosForm from "../components/molecules/orderpos-form";
import Product from "../components/molecules/product-classcomponent";
import Auth from "auth/Auth";

class OrderPos extends React.Component {
  constructor(props) {
    super(props);
    this.orderPos = React.createRef();
    this.product = React.createRef();
  }

  selectProduct = (p) => {
    this.orderPos.current.handleSelectProduct(p);
  };

  searchProduct = (event) => {
    event.preventDefault();
    this.product.current.handleSearchProduct(event.target.value);
  };

  setProductList = (productList) => {
    this.orderPos.current.handleSetProductList(productList);
  };

  setProductPackageList = (productPackageList) => {
    this.orderPos.current.handleSetProductPackageList(productPackageList);
  };

  render() {
    return (
      <Auth>
        <Head>
          <title>Order Pos</title>
        </Head>
        <div className="container-75">
          <div className="container-92">
            <div className="topnav">
              <div style={{ padding: "20px" }}>
                <ButtonBack style={"back-button"} route={"/"} />
              </div>
              <InputField
                classname={"search-box"}
                type={"search"}
                onChange={this.searchProduct}
                placeholder={"Search Box"}
              />
            </div>
            <div className="bottomnav">
              <Product
                ref={this.product}
                selectProduct={this.selectProduct}
                setProductList={this.setProductList}
                setProductPackageList={this.setProductPackageList}
              />
            </div>
          </div>
        </div>
        <OrderPosForm ref={this.orderPos} />
      </Auth>
    );
  }
}

export default OrderPos;
