import moment from 'moment'
import Head from 'next/head'
import React, { useState } from 'react'
import { HeaderPage, StartEndDate, TableReportProduct } from 'components'

const ProductReport = () => {
    const [startDate, setStartDate] = useState(moment(new Date()).subtract(7, 'day').format('YYYY-MM-DD'));
    const [endDate, setEndDate] = useState(moment(new Date()).format('YYYY-MM-DD'));

    console.log(startDate, 'ccc')


    const HandleStartDate = (startDate) => {
        setStartDate(startDate)
    }

    const HandleEndDate = (endDate) => {
        setEndDate(endDate)
    }

  return (
    <div>
        <Head>
            <title>Product Report</title>
        </Head>
        <div style={{backgroundColor:"white", height:"100vh"}}>
            <div>
                <div className='topnav'>
                    <HeaderPage href={"/report"}/>
                </div>
                <div style={{display:"flex", width:"87%", marginLeft:"6%"}}>
                            <StartEndDate
                                HandleStartDate = {HandleStartDate}
                                HandleEndDate = {HandleEndDate}
                                startDate={startDate}
                                marginLeft={"58%"}
                                color= {"#e8ebe1"}
                                endDate={endDate}
                                />
                </div>
                <TableReportProduct startDate={startDate} endDate={endDate}/>
            </div>     
        </div>
    </div>
  )
}

export default ProductReport




        //     <div className="container-utama" style={{backgroundColor:"red"}}>
                //     <div className="container-orderlist">
                //         <div className="topnav">
                //             <Link href="/">
                //                 <button className="back-button">Back</button>
                //             </Link>
                //         </div>
                //         <div className='bottomnav'>
                //             <h4 style={{margin:"20px", fontSize:"19 +px"}}>Order List</h4>

                //             <Sliderorder/>
                            
                //         </div>
                //     </div>
        // </div>