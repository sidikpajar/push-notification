import moment from 'moment'
import Head from 'next/head'
import React, { useState } from 'react'
import { HeaderPage, StartEndDate, TableReportTransaksi } from 'components'

const ProductReport = () => {
    const [startDate, setStartDate] = useState(moment(new Date()).subtract(7, 'day').format('YYYY-MM-DD'));
    const [endDate, setEndDate] = useState(moment(new Date()).format('YYYY-MM-DD'));

    const HandleStartDate = (startDate) => {
        setStartDate(startDate)
    }

    const HandleEndDate = (endDate) => {
        setEndDate(endDate)
    }
    return (
        <div>
            <Head>
                <title>Transaksi Report Report</title>
            </Head>
            <div style={{ backgroundColor: "white", height: "100vh" }}>
                <div>
                    <div className='topnav'>
                        <HeaderPage href={"/report"} />

                    </div>
                    <div style={{ display: "flex", width: "91%", marginLeft: "5%", marginRight: "5%" }}>
                        <StartEndDate
                            HandleStartDate = {HandleStartDate}
                            HandleEndDate = {HandleEndDate}
                            startDate={startDate}
                            color= {"#e8ebe1"}
                            marginLeft={"58%"}
                            endDate={endDate}
                            />
                    </div>
                    <TableReportTransaksi startDate={startDate} endDate={endDate}/>
                </div>
            </div>
        </div>
    )
}

export default ProductReport