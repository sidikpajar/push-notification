import React, { useState } from 'react'
import { FormReport } from 'components/organisms'
import Head from 'next/head'
import moment from 'moment'
import { HeaderPage, StartEndDate } from 'components'

const Report = () => {
    const [startDate, setStartDate] = useState(moment(new Date()).subtract(7, 'day').format('YYYY-MM-DD'))
    const [endDate, setEndDate] = useState(moment(new Date()).format('YYYY-MM-DD'))

    const HandleStartDate = (e) => {
        setStartDate(e)
    }

    const HandleEndDate = (e) => {
        setEndDate(e)
    }

    return (
        <div>
            <Head>
                <title>Report</title>
            </Head>
            <div style={{ backgroundColor: "white", height: "100vh" }}>
                <div>
                    <HeaderPage href={"/"} />
                    <div style={{ margin: "10px", marginLeft: "55px", marginTop: "30px", display: "flex" }}>
                        <div className='container-report' style={{ backgroundColor: "white", display: "flex"}}>
                            <StartEndDate
                                HandleStartDate={HandleStartDate}
                                HandleEndDate={HandleEndDate}
                                startDate={startDate}
                                marginLeft={"58%"}
                                color= {"#e8ebe1"}
                                endDate={endDate}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <FormReport startDate={startDate} endDate={endDate} HandleStartDate={HandleStartDate} />
                </div>
            </div>
        </div>
    )
}

export default Report