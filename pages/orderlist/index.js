import Head from 'next/head'
import React from 'react'
import { ButtonBack, OrderList } from '../../components'
import Auth from 'auth/Auth'

const index = () => {
  return (
    <Auth>
        <Head>
            <title>Order List</title>
        </Head>
        <div style={{backgroundColor:"#E5E5E5", height:"auto", paddingBottom:"30rem"}}>
            <div>
                <div style={{padding:"20px"}}>
                    <ButtonBack style={"back-button"} route={"/"}/>
                </div>
                <div style={{margin:"10px", marginLeft:"20px", marginTop:"30px"}}>
                    <h4 style={{fontSize:"19px"}}><b>Order List</b></h4>
                    <OrderList/>
                </div>
            </div>
        </div>
    </Auth>
  )
}

export default index