import { ButtonBack } from 'components'
import { ListProductSetting, MenuProductSetting } from 'components/molecules'
import Head from 'next/head'
import Auth from 'auth/Auth'
import React, { useState } from 'react'

const ProductSettings = () => {

    // eslint-disable-next-line no-unused-vars
    const [product, setProduct] = useState("")

    const selectProduct = (data) => {
        setProduct(data)
    }

  return (
    <Auth>
        <Head>
            <title>Product Setting</title>
        </Head>
       <div style={{display:"flex"}}>
            <div style={{backgroundColor:"white", height:"auto", paddingBottom:"30rem", width:"70%", justifyContent:"flex-end"}}>
                <div style={{padding:"20px"}}>
                        <ButtonBack style={"back-button"} route={"/"}/>
                    </div>
                    <div style={{ marginTop:"30px", width:"100%"}}>
                        <div>
                            <ListProductSetting selectProduct={selectProduct}/>
                        </div>
                    </div>
            </div>
            <div>
                <div style={{backgroundColor:"#E5E5E5", width:"30%", height:"100vh", position:"fixed"}}>
                    <MenuProductSetting/>
                </div>
            </div>
       </div>
    </Auth>
  )
}

export default ProductSettings