import { ButtonBack } from 'components'
import { FormProductPackage, ListProductSetting } from 'components/molecules'
import Head from 'next/head'
import React, { useState } from 'react'

const ProductPackageForm = () => {
    // eslint-disable-next-line no-unused-vars
    const [product, setProduct] = useState("")
    const selectProduct = (data) => {
        setProduct(data)
    }

    return (
        <div>
            <Head>
                <title>Product Setting</title>
            </Head>
            <div style={{ display: "flex" }}>
                <div style={{ backgroundColor: "white", height: "auto", paddingBottom: "30rem", width: "70%", justifyContent: "flex-end" }}>
                    <div style={{ padding: "20px" }}>
                        <ButtonBack style={"back-button"} route={"/product-setting/productsetting"} />
                    </div>
                    <div style={{ marginTop: "30px", width: "100%" }}>
                        <div>
                            <ListProductSetting selectProduct={selectProduct} />
                        </div>
                    </div>
                </div>
                <div>
                    <div style={{ backgroundColor: "#E5E5E5", width: "30%", height: "100vh", position: "fixed" }}>
                        <FormProductPackage />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductPackageForm