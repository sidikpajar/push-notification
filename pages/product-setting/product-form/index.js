import { ButtonBack, FormProduct, ListProductSetting } from 'components'
import Head from 'next/head'
import React, { useState } from 'react'
import Auth from 'auth/Auth'

const ProductForm = () => {
    const [product, setProduct] = useState("")

    const selectProduct = (data) => {
        setProduct(data)
    }

    return (
        <Auth>
            <Head>
                <title>Product Setting</title>
            </Head>
            <div style={{ display: "flex" }}>
                <div style={{ backgroundColor: "white", height: "auto", paddingBottom: "30rem", width: "70%", justifyContent: "flex-end" }}>
                    <div style={{ padding: "20px" }}>
                        <ButtonBack style={"back-button"} route={"/product-setting/productsetting"} />
                    </div>
                    <div style={{ marginTop: "30px", width: "100%" }}>
                        <div>
                            <ListProductSetting
                                selectProduct={selectProduct}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <div style={{ backgroundColor: "#E5E5E5", width: "30%", height: "100vh", position: "fixed", overflowY: "scroll" }}>
                        <FormProduct product={product} selectProduct={selectProduct} />
                    </div>
                </div>
            </div>
        </Auth>
    )
}

export default ProductForm