import Head from 'next/head'
import React from 'react'
import LoginForm from '../components/organisms/login-form/'
import Auth from 'auth/Auth'

class Login extends React.Component {
    render() {
        return (
            <Auth>
                <Head>
                    <title>Login</title>
                </Head>
                <div className="login-container">
                    <LoginForm />
                </div>
            </Auth>
        )
    }
}

export default Login;