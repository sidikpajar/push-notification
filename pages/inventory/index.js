import Head from "next/head";
import React from "react";
import { Inventorys } from "../../components";
import Auth from "auth/Auth";

const index = () => {
  return (
    <Auth>
      <Head>
        <title>Inventory</title>
      </Head>
      <Inventorys />
    </Auth>
  );
};

export default index;
