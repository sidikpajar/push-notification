import React from "react";
import Head from "next/head";
import { HeaderPage } from 'components';
import Auth from "auth/Auth";
import { ChartPaket, ChartProduct, ChartTransaksi } from "components/organisms";



const ReportDashboard = () => {
  return (
    <Auth>
      <Head>
        <title>Report</title>
      </Head>
      <div>
        <HeaderPage href={"/"} />
        <div style={{backgroundColor: 'white', width: '100vw', height: '100vh'}}>
          <div style={{flexDirection: 'row', display: 'flex', justifyContent: 'space-around'}}>
            <div>
              <ChartProduct />
            </div>
            <div>
              <ChartPaket />
            </div>
            <div>
              <ChartTransaksi />
            </div>
          </div>
         
         
          
        </div>
      </div>
    </Auth>
  );
}

export default ReportDashboard;
