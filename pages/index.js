import Head from "next/head";
import React from "react";
import Dashboard from "../components/organisms/dashboard/";
import Auth from "auth/Auth";


const Index = () => {

  return (
    <Auth>
      <div className="main-container">
        <Head>
          <title>Home</title>
        </Head>
        <div className="menu-container">
          <Dashboard />
        </div>
      </div>
    </Auth>
  );
};

export default Index;