import { ButtonBack } from "components";
import Head from "next/head";
import React from "react";
import OrderQueue from "../components/molecules/orderqueue";
import ProductionQueue from "../components/molecules/product-queue";
import Auth from "auth/Auth";
import moment from "moment";
import Router, { withRouter } from "next/router";

class ProdQueue extends React.Component {
  constructor(props) {
    super(props);
    this.productionQueue = React.createRef();
    this.orderQueue = React.createRef();
    this.state = {
      date: moment().format("YYYY-MM-DD"),
    };
  }

  searchProductionQueue = (event) => {
    event.preventDefault();
    this.productionQueue.current.handleSearchProductionQueue(
      event.target.value
    );
  };

  setDate = (event) => {
    event.preventDefault();
    Router.replace(
      {
        pathname: "/prodqueue",
        query: { date: event.target.value },
      },
      undefined,
      { shallow: true }
    );
  }

  render() {
    return (
      <Auth>
        <Head>
          <title>Production Queue</title>
        </Head>
        <div className="container-75">
          <div className="container-92">
            <div className="topnav">
              <div style={{
                  height: "fit-content",
                  marginTop: "auto",
                  marginBottom: "auto",
                  padding: '20px'
                }}>
                <ButtonBack route={"/"} />
              </div>
              <input
                className="search-box"
                type="search"
                onChange={this.searchProductionQueue}
                placeholder="Search Production Queue"
              ></input>
              <input
                type="date"
                value={this.props.router.query.date}
                style={{
                  backgroundColor: "white",
                  width: "100px",
                  marginRight: "15px",
                }}
                onChange={this.setDate}
              />
              <button
                onClick={
                  () =>
                    Router.reload()
                }
                style={{
                  height: "fit-content",
                  marginTop: "auto",
                  marginBottom: "auto",
                }}
              >
                Refresh
              </button>
            </div>
            <div className="bottomnav">
              <ProductionQueue
                ref={this.productionQueue}
                createdDate={this.state.date}
              />
            </div>
          </div>
        </div>
        <div className="container-25">
          <OrderQueue ref={this.orderQueue} deliveryDate={this.state.date} />
        </div>
      </Auth>
    );
  }
}

export async function getServerSideProps(context){
  if(!context?.query?.date){
    return {
      redirect: {
        destination: `/prodqueue?date=${moment().format('YYYY-MM-DD')}`,
        permanent: true,
      },
    }
  }
  return {
    props: {}
  }
}

export default withRouter(ProdQueue);
