import { createContext, useContext, useState } from "react";

const AppContext = createContext();

export function AppWrapper({ children }) {
  const [token, setToken] = useState(null);

  return (
    <AppContext.Provider
      value={{
        state: {
          token: token,
        },
        setToken: setToken,
      }}
    >
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
