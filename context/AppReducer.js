export const initialState = {
  token: "",
};

export const AppReducer = (state, action) => {
  switch (action.type) {
    case "set_token": {
      return {
        ...state,
        token: state.token,
      };
    }
  }
};
