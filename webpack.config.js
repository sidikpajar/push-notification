module.exports = {
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|mp3)$/i,
          loader: 'file-loader',
          options: {
            publicPath: 'assets',
          },
        },
      ],
    },
  };