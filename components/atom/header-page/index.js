import React from "react";
import { IoIosArrowBack } from 'react-icons/io';
import { useRouter } from "next/router";
import { Breadcrumbs } from "..";
import Link from "next/link";

const Index = ({href}) => {
  const router = useRouter();
  const dataBreadcrumbs = () => {
    const parentLink = router?.asPath?.split('/')[1];
    const childLink = router?.asPath?.split('/')[2];
    console.log(router?.asPath?.split('/')?.length);
    if (router?.asPath?.split('/')?.length > 2) {
      return [{
        label: parentLink,
        link: `/${parentLink}`
      }, {
        label: `${childLink} `,
        link: `${router?.asPath}`
      }]
    } else {
      return [{
        label: parentLink,
        link: `/${parentLink}`
      }]
    }
  }
  return (
    <>
      <div>
        <div style={{paddingTop:"20px"}}>
          <Link href={`${href}`}>
            <IoIosArrowBack style={{ fontSize: "30px", marginBottom: "10px", cursor:"pointer", marginLeft:"10PX"}} />
          </Link>
          <Breadcrumbs data={dataBreadcrumbs()} />
        </div>
      </div>
    </>
  )
}

export default Index;