import Link from 'next/link';
import React from 'react'
import { FaAngleRight } from "react-icons/fa";

const Index = ({data}) => {
    console.log(data)

    return (
        <>
            <div style={{margin:"20px", marginLeft:"4rem"}}>
                <Link href="/" style={{fontSize:"20px", color:"black", textDecoration:"none"}}><b>Dashboard</b></Link>
                {data?.map((item, index) => (
                    <>
                        <FaAngleRight/>
                        <Link key={index} style={{fontSize:"20px", color:"black", textDecoration:"none"}} href={item?.link}><b>{item?.label}</b></Link>
                    </>
                ))}
            </div>
        </>
    )
}

export default Index
