import Link from 'next/link'
import React from 'react'
import { IoIosArrowBack } from 'react-icons/io';

const Index = (props) => {
    const { route } = props
    return (
        <>
            <Link href={route}>
                <a>
                    <IoIosArrowBack style={{ color:"black", fontSize: "30px", marginBottom: "auto", cursor: "pointer", marginLeft: "auto" }} />
                </a>
            </Link>
            
        </>
    )
}

export default Index

