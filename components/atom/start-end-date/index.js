import React from 'react'

const Index = (props) => { 
    const {HandleStartDate, HandleEndDate, startDate, endDate, color, marginLeft} = props
    console.log(startDate, 'awdawd')
    return (
        <>
            <div style={{ display: "flex", flexDirection: "row", marginLeft: `${marginLeft}`, alignItems: "center" }}>
                <div>
                    <label style={{ padding: "10px" }}>Start Date</label>
                    <input type="date" style={{ margin: "10px", backgroundColor: `${color}` }}
                        // #e8ebe1
                        onChange={(e) => HandleStartDate(e.target.value)}
                        value={startDate}
                    />
                </div>
                <div>
                    <label style={{ padding: "10px" }}>End Date</label>
                    <input type="date" style={{ margin: "10px", backgroundColor: `${color}` }}
                        onChange={(e) => HandleEndDate(e.target.value)}
                        value={endDate}
                    />
                </div>
            </div>
        </>
    )
}

export default Index