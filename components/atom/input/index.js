import React from 'react'

const Index = ({classname, type, placeholder}) => {
  return (
   <>
          <input 
            className={classname}
            type={type}
            placeholder={placeholder}/>
   </>
  )
}

export default Index