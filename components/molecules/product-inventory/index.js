import React, { useEffect, useState } from "react";
import api from "api";

const Index = () => {
  const [inventory, setInventory] = useState([]);

  const getAllInventory = () => {
    try {
      api.get(`api/v1/admin/inventory`).then((response) => {
        let products = response?.data?.data?.result;
        setInventory(products);
        if (response.statusCode == 401) {
          console.log("error entut");
        } else {
          const products = response?.data?.data?.listInventory;
          setInventory(products);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllInventory();
  }, []);

  return (
    <>
      <div className="container-inventory-col-92">
        <div className="container-product-inventory">
          {inventory?.map((e, index) => (
            <div
              style={{ backgroundColor: "#F4F4F4", padding: "3rem" }}
              className="card-inventory"
              key={index}
            >
              <p
                style={{ textAlign: "center" }}
                className="title-nama-inventory"
              >
                {e.nama_inventory}
              </p>
              <p
                style={{ textAlign: "center" }}
                className="title-nama-inventory"
              >
                {e.jumlah} {e.satuan}
              </p>
              {/* <h1 style={{textAlign:"center"}} className='title-nama-inventory'>{e.jumlah}{e.satuan}</h1>
                    <h1 style={{textAlign:"center"}} className='title-nama-inventory'>{e.jumlah}{e.satuan}</h1> */}
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default Index;
