import React from "react";

const Index = ({onConfirm, onAbort}) => {
  return (
    <div>
      <div className="modal">
        <div className="modal-main-message-confimation">
          <div className="container-message-confirmation">
            <span style={{fontSize: '1em', fontWeight: 700}}>Apakah anda yakin ingin cancel order ini ?</span>
            <div style={{display:'flex', flexDirection: 'row',  justifyContent: 'space-between', marginTop: '1em'}}>
              <button onClick={onConfirm} style={{width: '40%', backgroundColor: "#95A97A", color: '#fff'}}>
                YA
              </button>
              <button onClick={onAbort} style={{width: '40%', backgroundColor: "#FF0000", color: "#fff"}}>
                TIDAK
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;
