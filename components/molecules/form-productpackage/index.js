import React, { useEffect, useState } from 'react'
import api from 'api';

const Index = () => {
    const [tambahInput, setTambahInput] = useState([]);
    const [product, setProduct] = useState([]);
    

    const handleAddInput = () => {
        console.log('click')
        const data = [...tambahInput]
        data.push(product[0]||[{}])
        setTambahInput(data)
    }

    const handleChange = (onChange, i) => {
        const data = [...tambahInput]
        console.log('cek', data)
        data[i] = onChange.target.value;
        setTambahInput(data)
        // console.log(inputdata)
    }

    const handleDelete = (i) => {
        const deleteInput = [...tambahInput]
        deleteInput.splice(i, 1)
        setTambahInput(deleteInput) 
    }

    const getAllInventory = () => {
        
        api.get(`api/v1/admin/product`)
        .then(response => {
            //console.log(response);
            let data = response.data;
            console.log(data);
            let _products = data.data.listProduct;
            setProduct(_products)
        })
        .catch(error => {
            console.log(error);
            
        })
    }

    useEffect(() => {
        getAllInventory()
    }, [])
    return (
        <div>
            <div className="inner-container-25">
                <form style={{ margin: "-10px", marginBottom: "10px" }}>
                    <div style={{ display: "grid", paddingLeft: "2px" }}>
                        <div style={{ display: "flex" }}>
                            <input
                                type="file"
                                className="form-input"
                                // onChange={loadImage}
                                style={{ backgroundColor: "#E5E5E5", width: "47%" }} />
                            {/* #E5E5E5 */}
                            <h2 style={{ margin: "10px", cursor: "pointer", color: "red" }}>x</h2>
                        </div>
                    </div>
                </form>


                <form className="order-form" style={{ margin: "5px" }} id="form-input-order">
                    <div>
                        <label>Nama Product</label>
                        <input
                            type="text"
                            className="form-input"
                            name="nama_user" />
                    </div>
                    <div>
                        <small></small>
                    </div>
                    <div>
                        <label>Harga Product</label>
                        <input
                            type="tel"
                            className="form-input"
                            name="no_handphone" />
                    </div>
                    <div>
                        <small></small>
                    </div>
                </form>

                <form className="order-form" style={{ margin: "5px", marginTop: "20px" }} id="form-input-order">
                    <div style={{ display: "flex" }}>
                        <h3>ISI PACKAGE</h3>
                        <div style={{ marginTop: "-10px", marginLeft: "64%", cursor: "pointer" }}>
                            <h1 onClick={() => handleAddInput()}>+</h1>
                        </div>
                    </div>

                    {tambahInput.map((data, i) =>
                        <div style={{ marginTop: "15px", display: "flex"}} key={i}>
                            <select style={{backgroundColor: "#e8ebe1", borderRadius: ".5rem", width: "100%", height: "50px" }}
                            onChange={e => handleChange(e, i)}
                            value={data}
                            >
                                {product.map((item, index) => 
                                <option key={index} style={{textAlign:"left"}} value={item.id}>{item.nama_product}</option>)}
                                
                            </select>
                            <div style={{ display: "flex" }}>
                                <h2 onClick={()=> handleDelete(i)} style={{ margin: "10px", marginLeft:"20px" ,marginTop: "10px", cursor: "pointer" }}>x</h2>
                            </div>
                            <div>
                                <small></small>
                            </div>
                        </div>)}
                </form>


            </div>
            <div className="submit-button-footer">
                <button
                    className="submit-button"
                >
                    Create Order
                </button>
            </div>
        </div>
    )
}

export default Index