import React from 'react'
import api from 'api';

class Product extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            _products: [],
            listProductPackage: [],
            _listProductPackage: [],
        }

    }

    componentDidMount(){

        this.getProduct();
    }

    async getProduct() {
        // Ala Carte
        api.get(`api/v1/admin/product`)
        .then(response => {
            //console.log(response);
            let data = response.data;
            console.log(data);
            let _products = data.data.listProduct;
            let rows = [...Array(Math.ceil(_products.length / 4))];
            let productRows = rows.map((row, index) => _products.slice(index * 4, index * 4 + 4));
            let products = productRows.map((row, index) => (
                <div className="product-list-row" key={index}>
                    {row.map(p => (
                        <div className="product-list-col" key={p.productID}>
                            <div className="product-container">
                                <img src={p.product_image_url} alt="juice.png" onClick={(event) => this.selectProduct(event, p)}></img>
                                <p className="text-block">{p.nama_product}</p>
                            </div>
                        </div>
                    ))}
                </div>
            ));
            
            this.props.setProductList(_products);
            
            this.setState({ products: products, _products: _products })
        })
        .catch(error => {
            console.log(error);
            
        })

        // Package
        api.get(`api/v1/admin/product_package`)
        .then(response => {
            //console.log(response);
            let data = response.data;
            let _listProductPackage = data.data.listProductPackage;
            let rows = [...Array(Math.ceil(_listProductPackage.length / 4))];
            let productPackageRows = rows.map((row, index) => _listProductPackage.slice(index * 4, index * 4 + 4));
            let listProductPackage = productPackageRows.map((row, index) => (
                <div className="product-list-row" key={index}>
                    {row.map(p => (
                        <div className="product-list-col" key={p.product_packageID}>
                            <div className="product-container">
                                <img src={p.product_package_image} alt="juice.png" onClick={(event) => this.selectProduct(event, p)}></img>
                                <p className="text-block">{p.nama_product_package}</p>
                            </div>
                        </div>
                    ))}
                </div>
            ));

            this.props.setProductPackageList(_listProductPackage);

            this.setState({ listProductPackage: listProductPackage, _listProductPackage: _listProductPackage })
        })
        .catch(error => {
            console.log(error);
        })
    }

    handleSearchProduct(keyword) {
        let _products = [];
        let _listProductPackage = [];

        for (let i = 0; i < this.state._products.length; i++) {
            if (this.state._products[i].nama_product.includes(keyword)) {
                _products.push(this.state._products[i]);
            }
        }
        
        for (let i = 0; i < this.state._listProductPackage.length; i++) {
            if (this.state._listProductPackage[i].nama_product_package.includes(keyword)) {
                _listProductPackage.push(this.state._listProductPackage[i]);
            }
        }

        let rows = [...Array(Math.ceil(_products.length / 4))];
        let productRows = rows.map((row, index) => _products.slice(index * 4, index * 4 + 4));
        let products = productRows.map((row, index) => (
            <div className="product-list-row" key={index}>
                {row.map(p => (
                    <div className="product-list-col" key={p.productID}>
                        <div className="product-container">
                            <img src={p.product_image_url} alt="juice.png" onClick={(event) => this.selectProduct(event, p)}></img>
                            <p className="text-block">{p.nama_product}</p>
                        </div>
                    </div>
                ))}
            </div>
        ));

        let rows2 = [...Array(Math.ceil(_listProductPackage.length / 4))];
        let productPackageRows = rows2.map((row, index) => _listProductPackage.slice(index * 4, index * 4 + 4));
        let listProductPackage = productPackageRows.map((row, index) => (
            <div className="product-list-row" key={index}>
                {row.map(p => (
                    <div className="product-list-col" key={p.product_packageID}>
                        <div className="product-container">
                            <img src={p.product_package_details[0].product_image_url} alt="juice.png" onClick={(event) => this.selectProduct(event, p)}></img>
                            <p className="text-block">{p.nama_product_package}</p>
                        </div>
                    </div>
                ))}
            </div>
        ));
        
        this.setState({ products: products, listProductPackage: listProductPackage })
    }

    selectProduct = (event, p) => {
        event.preventDefault();
        this.props.selectProduct(p);
    }

    render() {
        return (
            <div>
                <div className='product-title-text'>ALACARTE</div>
                {this.state.products}

                <div className='product-title-text'>PACKAGE</div>
                {this.state.listProductPackage}
            </div>
        )
    }
}

export default Product