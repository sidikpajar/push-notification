import React, { useEffect, useState } from "react";
import api from "api";
import Loading from "../../../libs/Loading";

const Index = (props) => {
  const { selectProduct } = props;

  const [product, setProduct] = useState([]);
  const [productPackage, setProductPackage] = useState([]);
  const [loading, setLoading] = useState(false);

  // console.log('oooppp', clickHandle)

  const getAllInventory = () => {
    setLoading(true);
    api
      .get(`api/v1/admin/product`)
      .then((response) => {
        //console.log(response);
        let data = response.data;
        console.log(data);
        let _products = data.data.listProduct;
        let rows = [...Array(Math.ceil(_products.length / 4))];
        let productRows = rows.map((row, index) =>
          _products.slice(index * 4, index * 4 + 4)
        );
        let products = productRows.map((row, index) => (
          <div className="product-list-row" key={index}>
            {row.map((p) => (
              <div className="product-list-col" key={p.productID}>
                <div className="product-container">
                  <img
                    src={p.product_image_url}
                    alt="juice.png"
                    onClick={() => selectProduct(p)}
                  ></img>
                  <p className="text-block">{p.nama_product}</p>
                </div>
              </div>
            ))}
          </div>
        ));
        setProduct(products);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });

    // Package
    api
      .get(`api/v1/admin/product_package`)
      .then((response) => {
        //console.log(response);
        let data = response.data;
        let _listProductPackage = data.data.listProductPackage;
        let rows = [...Array(Math.ceil(_listProductPackage.length / 4))];
        let productPackageRows = rows.map((row, index) =>
          _listProductPackage.slice(index * 4, index * 4 + 4)
        );
        let listProductPackage = productPackageRows.map((row, index) => (
          <div className="product-list-row" key={index}>
            {row.map((p) => (
              <div className="product-list-col" key={p.product_packageID}>
                <div className="product-container">
                  <img
                    src={p.product_package_image}
                    alt="juice.png"
                    onClick={() => selectProduct(p)}
                  ></img>
                  <p className="text-block">{p.nama_product_package}</p>
                </div>
              </div>
            ))}
          </div>
        ));

        setProductPackage(listProductPackage);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getAllInventory();
  }, []);

  return (
    <div>
      <div style={{ marginLeft: "15px" }}>
        {loading ? (
          <div id="file_img">
            <Loading />
          </div>
        ) : (
          <div>
            <div className="product-title-text">ALACARTE</div>
            {product}

            <div className="product-title-text">PACKAGE</div>
            {productPackage}
          </div>
        )}
      </div>
    </div>
  );
};

export default Index;
