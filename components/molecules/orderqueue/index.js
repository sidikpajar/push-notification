import api from "api";
import React from "react";
import { withRouter } from "next/router";

class OrderQueue extends React.Component {
  state = {
    orderQueue: [],
  };

  callbackGrouping({ products }) {
    return products.product_packageID;
  }

  soundPlay() {
    let audio = new Audio("./bell2.mp3");
    audio.play();
  }

  componentDidUpdate(prevProps) {
    if (this.props.router !== prevProps.router) {
      this.getOrderQueue();
    }
  }

  getOrderQueue() {
    api
      .get(`api/v1/admin/order?tgl_pengiriman=${this.props.router.query.date}`)
      .then((response) => {
        let data = response.data;
        let result = data.data.result;
        let orderQueue = result.map((o, index) => {
          if (o.status_pengiriman !== "Cancel") {
            return (
              <div className="order-queue-row" key={index}>
                <div className="order-queue-col">
                  <div className="order-queue-container">
                    <div
                      style={{
                        width: "100%",
                        textOverflow: "ellipsis",
                        marginBottom: "15px",
                        overflow: "hidden",
                      }}
                    >
                      <span>
                        <b>
                          {o.nama_user} - {o.transaction_code}
                        </b>
                      </span>
                    </div>
                    <p
                      className={
                        o.status_pengiriman === "Ready"
                          ? "order-queue-text-status-ready-que"
                          : "order-queue-text-status-waiting-que"
                      }
                    >
                      <b>{o.status_pengiriman}</b>
                    </p>
                    <div style={{ display: "flex", fontSize: "5px" }}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          width: "50%",
                        }}
                      >
                        <div
                          style={{ alignItems: "flex-end", marginLeft: "0px" }}
                        >
                          <p style={{ fontSize: "15px" }}>Pengiriman:</p>
                          <p style={{ fontSize: "15px" }}>
                            <b>{o.metode_pengiriman}</b>
                          </p>
                        </div>
                        <div
                          style={{ alignItems: "flex-end", marginLeft: "0px" }}
                        >
                          <p style={{ fontSize: "15px" }}>Tipe Order:</p>
                          <p style={{ fontSize: "15px" }}>
                            <b>{o.tipe_order}</b>
                          </p>
                        </div>
                      </div>
                      <div
                        style={{
                          marginLeft: "20%",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                          width: "60%",
                        }}
                      >
                        <p style={{ fontSize: "14px", marginLeft: "5px" }}>
                          Tanggal Pesan:
                        </p>
                        <p
                          style={{
                            fontSize: "17px",
                            marginTop: "2px",
                            marginLeft: "10px",
                          }}
                        >
                          <b>{o.tgl_pengiriman}</b>
                        </p>
                      </div>
                    </div>

                    <p className="order-queue-text-title">Notes:</p>

                    <p className="order-queue-text-value">
                      <b>{o.note}</b>
                    </p>

                    <p className="divider">=================</p>
                    <div className="order-queue-item">
                      <div></div>
                      {o.products.map((p, index) => {
                        if (p.product_packageID === null) {
                          return (
                            <p>
                              {p.nama_product} x {p.quantity}
                            </p>
                          );
                        }
                        if (typeof o.products[index - 1] != "undefined") {
                          if (
                            o.products[index].product_packageID ===
                            o.products[index - 1].product_packageID
                          ) {
                            return (
                              <li key={p.detail_transaksiID}>
                                {p.nama_product} x {p.quantity}
                              </li>
                            );
                          } else {
                            return (
                              <div key={index}>
                                <p>------------------------</p>
                                {p.nama_product_package}
                                <li key={p.detail_transaksiID}>
                                  {p.nama_product} x {p.quantity}
                                </li>
                              </div>
                            );
                          }
                        } else {
                          return (
                            <div>
                              {p.nama_product_package}

                              <li key={p.detail_transaksiID}>
                                {p.nama_product} x {p.quantity}
                              </li>
                            </div>
                          );
                        }
                      })}
                    </div>
                    <div className="order-queue-container-row">
                      <button
                        className={
                          o.status_pengiriman === "Ready"
                            ? "button-dones"
                            : "button-done-disabled"
                        }
                        type="button"
                        style={{ margin: "10px" }}
                        onClick={(event) => this.updateOrderQueue(event, o)}
                      >
                        DONE
                      </button>
                      <button
                        // button-pickup
                        className={
                          o.status_pengiriman === "Ready"
                            ? "button-done-disabled"
                            : "button-pickup"
                        }
                        type="button"
                        style={{ margin: "10px" }}
                        onClick={(event) => this.readyToPickup(event, o)}
                      >
                        Ready Pickup
                      </button>
                      {/* <button onClick={(event) => this.soundPlay(event, o)}
                      >Sound</button> */}
                    </div>
                  </div>
                </div>
              </div>
            );
          }
        });

        this.setState({ orderQueue });
        //PUSH KE LOCAL STORAGE
        localStorage.setItem("orderque", JSON.stringify(orderQueue));
        // localStorage.setItem("orderque", orderQueue);

        // console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  }


  updateOrderQueue = (event, o) => {
    event.preventDefault();
    api
      .put(`api/v1/admin/order_done/${o.transaksiID}`)
      .then(() => {
        // console.log(response);
        this.getOrderQueue();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  readyToPickup = (event, o) => {
    event.preventDefault();
    api
      .put(`api/v1/admin/ready_to_pickup/${o.transaksiID}`)
      .then(() => {
        //console.log(response);
        this.getOrderQueue();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  componentDidMount() {
    this.getOrderQueue();
  }

  render() {
    return (
      <>
        {/* <audio id="soundid" src="./bell2.mp3"></audio> */}
        <div className="inner-container-25-queue">{this.state.orderQueue}</div>
      </>
    );
  }
}

export default withRouter(OrderQueue);
