import React, { useEffect, useState } from 'react'
import PaginationProduct from '../pagination';
import api from 'api';
import { PopupData } from '..';

const TableReportProduct = ({startDate, endDate}) => {
  const [dataProduk, setDataProduk] = useState([])
  // eslint-disable-next-line no-unused-vars
  const [loading, setLoading] = useState([])
  const [search, setSearch] = useState("")
 

  const getAllInventory = () => {
    try {
      setLoading(true) 
      // &search=Car
      api.get(`api/v1/admin/report/product?date_from=${startDate}&date_end=${endDate}&search=${search}`)
        .then((response) => {
          console.log(response)

          if(response.statusCode == 401) {
              console.log('Harus relogin')
          } else {
              const products = (response?.data?.data?.reportProductionDone)
              setDataProduk(products)
              setLoading(false)
          }   
        })
    } catch (error) {
      console.log(error)
    }
  }

  const ExportToCSV = () => {
    try {
      api({
        url: `api/v1/admin/report/excel_production`,
        method: 'POST',
        responseType: 'blob', // important
        })
      .then((response) => {
        const outputFilename = `Data-Product.xls`;

        // If you want to download file automatically using link attribute.
        const url = URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', outputFilename);
        document.body.appendChild(link);
        link.click();
      })
    } catch (error) {
      console.log('err', error)
    }
  }

  const handleSearch = (e) => {
    setSearch(e)
  }

  useEffect(() => {
    getAllInventory()
  }, [startDate, endDate, search])


  return (
    <React.Fragment>
        <div style={{ display: "flex", marginLeft: "8%" }}>
          <div style={{ display: "flex" }}>
            <label style={{ padding: "10px", display: "flex", alignItems: "center" }}><b>Search</b></label>
            <input 
              type="text" 
              style={{ margin: "10px", backgroundColor: "#e8ebe1", borderRadius: "7rem" }}
              onChange={(e)=> handleSearch(e.target.value)}
              
            />
          </div>
          <div style={{ display: "flex", height: "40px", marginTop: "10px" }}>
            <label style={{ padding: "10px", display: "flex", alignItems: "center" }}><b>Number of rows</b></label>
            <select style={{ backgroundColor: "#e8ebe1", borderRadius: ".7rem" }}
            // onChange={(e) => changeStatus(e.target.value)}
            // value={status} 
            >
              <option value={'Waiting'}>10</option>
              <option value={'Ready'}>25</option>
              <option value={'Done'}>50</option>
            </select>
          </div>
          <div style={{ width: "53%", display: "flex", justifyContent: "flex-end", marginRight: "3rem" }}>
            {/* <ExportToExcel dataProduk= {dataProduk}/> */}
            <button onClick={ExportToCSV} style={{ width: "100px", height: "40px", marginTop: "10px", backgroundColor: "#9FB866", color: "white", cursor: "pointer" }}>Export</button>
          </div>
        </div>

        <div style={{ width: "100%", display: "flex" }}>
          <div className='whitescroll'>
            <table className='table-report-product'>
              <thead>
                <tr>
                  <th className='table-head row-left'>No</th>
                  <th className='table-head'>Nama</th>
                  <th className='table-head'>Terbuat</th>
                </tr>
              </thead>

              <tbody>
                {dataProduk.map((e, index) =>
                  <tr key={index}>
                    <td className='table-body row-left'>{index + 1}</td>
                    <td className='table-body'>{e.nama_production_queue}</td>
                    <td className='table-body'>{e.total_quantity}</td>
                  </tr>)}
              </tbody>
            </table>
           {dataProduk.length === 0 && <PopupData width={"350px"} height={"250px"}/>}
          </div>
        </div>
        

        <PaginationProduct/>
    </React.Fragment>
  )
}

export default TableReportProduct