import React from 'react'

class ModalWindow extends React.Component {
    state = {
        openModal: false,
    }

    showModal() {
        this.setState({ openModal: true })
    }

    closeModal() {
        this.setState({ openModal: false })
    }

    closeModalButton = (event) => {
        event.preventDefault();
        this.setState({ openModal: false })
    }
 
    render() {
        if (this.state.openModal) {
            return (
                <div className="modal">
                    <div className="modal-main">
                        <div className="modal-main-row">
                            <button onClick={this.closeModalButton}>Close</button>
                        </div>
                        {this.props.children}
                    </div>
                </div>
            )
        }
        else {
            return (null)
        }
    }
}

export default ModalWindow;