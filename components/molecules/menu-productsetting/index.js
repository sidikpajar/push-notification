import Link from 'next/link'
import React from 'react'

const Index = () => {
  return (
    <div >
      <div style={{ display: "grid", margin: "40px" }}>
        <Link href="/product-setting/product-form">
          <button style={{ height: "50px", cursor: "pointer" }}>ADD PRODUCT</button>
        </Link>
        <Link href="/product-setting/productpackage-form">
          <button style={{ height: "50px", marginTop: "10px", cursor: "pointer" }}>ADD PRODUCT PACKAGE</button>
        </Link>
      </div>
    </div>
  )
}

export default Index