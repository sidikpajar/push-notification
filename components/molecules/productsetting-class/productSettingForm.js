import api from 'api';
import React from 'react'

class ProductSettingForm extends React.Component {
    state = {
        productID: '',
        productImage: null,
        nama_product: '',
        harga_product: '',
        jenis_product: '',
        isi_product: '',
        formErrors: { productImage: '', nama_product: '', harga_product: '', jenis_product: '', isi_product: '' },
        productImageValid: false,
        namaProductValid: false,
        hargaProductValid: false,
        jenisProductValid: false,
        isiProductValid: false,
        formValid: false,
    };

    handleSelectProduct(p) {
        this.setState({
            productID: p.productID,
            productImage: p.productImage, // null
            nama_product: p.nama_product,
            harga_product: p.harga_product,
            jenis_product: p.jenis_product,
            isi_product: p.isi_product,
            formErrors: { productImage: '', nama_product: '', harga_product: '', jenis_product: '', isi_product: '' },
            productImageValid: true,
            namaProductValid: true,
            hargaProductValid: true,
            jenisProductValid: true,
            isiProductValid: true,
            formValid: true,
        })
    }

    validateField(fieldName, value) {
        let formErrors = this.state.formErrors;
        let productImageValid = this.state.productImageValid;
        let namaProductValid = this.state.namaProductValid;
        let hargaProductValid = this.state.hargaProductValid;
        let jenisProductValid = this.state.jenisProductValid;
        let isiProductValid = this.state.isiProductValid;

        switch (fieldName) {
            case "productImage":
                productImageValid = value != null;
                formErrors.productImage = productImageValid ? "" : "Image is required";
                break;
            case "nama_product":
                namaProductValid = value.length >= 3;
                formErrors.nama_product = namaProductValid ? "" : "Name is too short";
                break;
            case "harga_product":
                hargaProductValid = value > 0;
                formErrors.harga_product = hargaProductValid ? "" : "Price amount must be greater than 0";
                break;
            case "jenis_product":
                jenisProductValid = value.length > 5;
                formErrors.jenis_product = jenisProductValid ? "" : "Type is too short";
                break;
            case "isi_product":
                isiProductValid = value.length > 5;
                formErrors.isi_product = isiProductValid ? "" : "Content is too short";
                break;
            default:
                break;
        }

        this.setState({
            formErrors: formErrors,
            productImageValid: productImageValid,
            namaProductValid: namaProductValid,
            hargaProductValid: hargaProductValid,
            jenisProductValid: jenisProductValid,
            isiProductValid: isiProductValid}, this.validateForm)
    }

    validateForm() {
        this.setState({ formValid:
            this.state.productImageValid &&
            this.state.namaProductValid &&
            this.state.hargaProductValid &&
            this.state.jenisProductValid &&
            this.state.isiProductValid
        })
    }

    handleChange = (event) => {
        event.preventDefault();
        let name = event.target.name;
        switch (name) {
            case "productImage":
                // eslint-disable-next-line no-case-declarations
                let file = event.target.files[0];
                this.setState({ [name]: file }, () => { this.validateField(name, file) })
                break;
            default:
                // eslint-disable-next-line no-case-declarations
                let value = event.target.value;
                this.setState({ [name]: value }, () => { this.validateField(name, value) })
                break;
        }
    };
    
    handleSubmit = (event) => {
        event.preventDefault();

        let { productID, productImage, nama_product, harga_product, jenis_product, isi_product } = this.state;

        let formData = new FormData();
        formData.append('productImage', productImage);
        formData.append('nama_product', nama_product);
        formData.append('harga_product', harga_product);
        formData.append('jenis_product', jenis_product);
        formData.append('isi_product', isi_product);

        if (productID == "")
        {
            api.post(`api/v1/admin/add_product`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then(() => {
                this.props.getProduct();
            }).catch(error => {
                console.log(error);
            });
        }
        else {
            api.put(`api/v1/admin/product/${productID}`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then(() => {
                this.props.getProduct();
            }).catch(error => {
                console.log(error);
            });
        }

        this.setState({
            productID: '',
            productImage: null,
            nama_product: '',
            harga_product: '',
            jenis_product: '',
            isi_product: '',
            formErrors: { productImage: '', nama_product: '', harga_product: '', jenis_product: '', isi_product: '' },
            productImageValid: false,
            namaProductValid: false,
            hargaProductValid: false,
            jenisProductValid: false,
            isiProductValid: false,
            formValid: false,
        })
    };

    render() {
        return (
            <div>
                <form className="product-form">
                    <div>
                        <input type="file"
                        className="form-input"
                        name="productImage"
                        onChange={this.handleChange}
                        accept="image/*"
                        />
                    </div>
                    <div>
                        <small>{this.state.formErrors.productImage}</small>
                    </div>
                    <div><label>Nama Product</label></div>
                    <div>
                        <input type="text"
                        className="form-input"
                        name="nama_product"
                        value={this.state.nama_product}
                        onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <small>{this.state.formErrors.nama_product}</small>
                    </div>
                    <div><label>Harga Product</label></div>
                    <div>
                        <input type="number"
                        className="form-input"
                        name="harga_product"
                        value={this.state.harga_product}
                        onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <small>{this.state.formErrors.harga_product}</small>
                    </div>
                    <div><label>Jenis Product</label></div>
                    <div>
                        <input type="text"
                        className="form-input"
                        name="jenis_product"
                        value={this.state.jenis_product}
                        onChange={this.handleChange}  
                        />
                    </div>
                    <div>
                        <small>{this.state.formErrors.jenis_product}</small>
                    </div>
                    <div><label>Isi Product</label></div>
                    <div>
                        <input type="text"
                        className="form-input"
                        name="isi_product"
                        value={this.state.isi_product}
                        onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <small>{this.state.formErrors.isi_product}</small>
                    </div>
                </form>
                <div className="submit-button-footer">
                    <button className="submit-button" disabled={!this.state.formValid} onClick={this.handleSubmit}>Submit / Update</button>
                </div>                
            </div>
        )
    }
}

export default ProductSettingForm;