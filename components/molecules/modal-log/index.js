import React, { useEffect, useState } from "react";
import api from "api";
import Moment from "moment";

const Index = ({ closeModal }) => {
  const [inventory, setInventory] = useState([]);

  // console.log('Inventory',inventory);

  const getAllInventory = () => {
    try {
      api.get(`api/v1/admin/log_inventory`).then((response) => {
        if (response.statusCode == 401) {
          console.log("Harus Relogin");
        } else {
          let products = response?.data?.data?.result;
          setInventory(products);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllInventory();
  }, []);

  return (
    <div>
      <div className="modal">
        <div className="modal-main-edit-inventory">
          <div className="modal-main-row">
            <button onClick={() => closeModal(false)}>Close</button>
          </div>

          <div className="container-product-log">
            <form className="product-form">
              <h2>Log Inventory</h2>

              {inventory?.map((e, index) => (
                <div
                  style={{ columnGap: "10px", marginBottom: "5px" }}
                  key={index}
                >
                  <div key={index}>
                    <div style={{ display: "flex" }}>
                      <h3>
                        {Moment(e.created_at).format("D MMMM YYYY, h:mm a")}
                      </h3>{" "}
                      <br />
                    </div>
                    {e.logDetail.map((o, index) => (
                      <div style={{ display: "grid" }} key={index}>
                        <h3 style={{ color: "#666666" }}>
                          {o.nama_inventory} {o.jumlah} {o.satuan}
                        </h3>
                      </div>
                    ))}
                    <hr />
                  </div>
                </div>
              ))}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;
