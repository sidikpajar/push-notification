import api from "api";
import React from "react";
import ModalWindow from "../modal";
import { withRouter } from "next/router";

class ProductionQueue extends React.Component {
  constructor(props) {
    super(props);
    this.productionQueueModal = React.createRef();
    this.state = {
      selectedProductionQueue: { nama_production_queue: "", productID: "" },
      limit: 0,
      productionQueue: [],
      _listProductionQueue: []
    };
    this.getProductionQueue();
  }

  componentDidUpdate(prevProps) {
    if(this.props.router !== prevProps.router){
      this.getProductionQueue()
    }
  }

  componentDidMount() {
    this.getProductionQueue()
  }
  
  async getProductionQueue() {
    api
      .get(`api/v1/admin/production_queue_not_done?tgl_pengiriman=${this.props.router.query.date}`)
      .then((response) => {
        //console.log(response);
        let data = response.data;
        let _listProductionQueue = data.data.productionQueueNotDone;
        let rows = [...Array(Math.ceil(_listProductionQueue.length / 2))];
        let productionQueueRows = rows.map((row, index) =>
          _listProductionQueue.slice(index * 2, index * 2 + 2)
        );
        let productionQueue = productionQueueRows.map((row, index) => (
          <div className="production-queue-row" key={index}>
            {row.map((pq) => (
              <div className="production-queue-col" key={pq.production_queueID}>
                <div className="production-queue-container">
                  <div className="production-queue-container-row">
                    <p>
                      <b>{pq.nama_production_queue}</b>
                    </p>
                  </div>
                  <div className="production-queue-container-row">
                    <div className="production-queue-container-col">
                      <button
                        className="production-queue-prog-button"
                        onClick={(event) =>
                          this.showProductionQueueModal(event, pq)
                        }
                      >
                        Prog
                      </button>
                    </div>
                    <div className="production-queue-container-col">
                      <p>
                        <b>{pq.total_quantity}</b>
                      </p>
                    </div>
                    <div className="production-queue-container-col">
                      <button
                        className="production-queue-done-button"
                        onClick={(event) => this.handleDoneButton(event, pq)}
                      >
                        Done
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ));

        this.setState({
          productionQueue: productionQueue,
          _listProductionQueue: _listProductionQueue,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleSearchProductionQueue(keyword) {
    let _listProductionQueue = [];

    for (let i = 0; i < this.state._listProductionQueue.length; i++) {
      if (
        this.state._listProductionQueue[i].nama_production_queue.includes(
          keyword
        )
      ) {
        _listProductionQueue.push(this.state._listProductionQueue[i]);
      }
    }

    let rows = [...Array(Math.ceil(_listProductionQueue.length / 2))];
    let productionQueueRows = rows.map((row, index) =>
      _listProductionQueue.slice(index * 2, index * 2 + 2)
    );
    let productionQueue = productionQueueRows.map((row, index) => (
      <div className="production-queue-row" key={index}>
        {row.map((pq) => (
          <div className="production-queue-col" key={pq.production_queueID}>
            <div className="production-queue-container">
              <div className="production-queue-container-row">
                <p>
                  <b>{pq.nama_production_queue}</b>
                </p>
              </div>
              <div className="production-queue-container-row">
                <div className="production-queue-container-col">
                  <button
                    className="production-queue-prog-button"
                    onClick={(event) =>
                      this.showProductionQueueModal(event, pq)
                    }
                  >
                    Prog
                  </button>
                </div>
                <div className="production-queue-container-col">
                  <p>
                    <b>{pq.total_quantity}</b>
                  </p>
                </div>
                <div className="production-queue-container-col">
                  <button
                    className="production-queue-done-button"
                    onClick={(event) => this.handleDoneButton(event, pq)}
                  >
                    Done
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    ));

    this.setState({ productionQueue: productionQueue });
  }

  handleLimitButton = (event, limit) => {
    event.preventDefault();
    this.setState({ limit: limit });
  };

  handleProgressButton = (event, productID, limit) => {
    event.preventDefault();
    api
      .put(
        `api/v1/admin/production_queue_limit/${productID}/${limit}`
      )
      .then(() => {
        //console.log(response);
        console.log("Production queue updated successfully");
        this.getProductionQueue();
        this.productionQueueModal.current.closeModal();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleDoneButton = (event, pq) => {
    event.preventDefault();
    api
      .put(
        `api/v1/admin/production_queue_done/${pq.productID}`
      )
      .then(() => {
        //console.log(response);
        console.log("Production queue updated successfully");
        this.getProductionQueue();
      })
      .catch((error) => {
        console.log(error);
      });

    this.getProductionQueue();
  };

  showProductionQueueModal = (event, pq) => {
    event.preventDefault();

    let productionQueue = {
      nama_production_queue: pq.nama_production_queue,
      productID: pq.productID,
    };

    this.setState({ selectedProductionQueue: productionQueue });

    this.productionQueueModal.current.showModal();
  };


  render() {
    return (
      <div>
        <ModalWindow ref={this.productionQueueModal}>
          <div className="production-queue-modal-container-row">
            <label>
              <b>{this.state.selectedProductionQueue.nama_production_queue}</b>
            </label>
          </div>
          <div className="production-queue-modal-container-row">
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 1 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 1)}
            >
              1
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 2 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 2)}
            >
              2
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 3 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 3)}
            >
              3
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 4 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 4)}
            >
              4
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 5 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 5)}
            >
              5
            </button>
          </div>
          <div className="production-queue-modal-container-row">
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 6 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 6)}
            >
              6
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 7 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 7)}
            >
              7
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 8 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 8)}
            >
              8
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 9 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 9)}
            >
              9
            </button>
            <button
              className={
                "production-queue-limit-button" +
                (this.state.limit == 10 ? "-active" : "")
              }
              onClick={(event) => this.handleLimitButton(event, 10)}
            >
              10
            </button>
          </div>
          <div className="production-queue-modal-container-row">
            <button
              onClick={(event) =>
                this.handleProgressButton(
                  event,
                  this.state.selectedProductionQueue.productID,
                  this.state.limit
                )
              }
            >
              Update
            </button>
          </div>
        </ModalWindow>
        {this.state.productionQueue}
      </div>
    );
  }
}

export default withRouter(ProductionQueue);
