import React, { useEffect, useState } from "react";
import PaginationProduct from "../pagination";
import api from "api";
import { ModalTransaksiReport, PopupData } from "..";

const TableReportTransaksi = ({ startDate, endDate }) => {
  const [openTransaksi, setOpenTransaksi] = useState(false);
  const [dataTransaksi, setDataTransaksi] = useState([]);
  const [TransaksiId, setTransaksiId] = useState("");
  const [search, setSearch] = useState("");
  // eslint-disable-next-line no-unused-vars
  const [loading, setLoading] = useState(false);

  // console.log("datatransaksi", endDate)
  // console.log("startdate123", search)

  const getAllInventory = () => {
    try {
      setLoading(true);
      api
        .get(
          `api/v1/admin/report/transaksi?date_from=${startDate}&date_end=${endDate}&search=${search}`
        )
        .then((response) => {
          console.log(response);
          if (response.statusCode == 401) {
            console.log("harus relogin");
          } else {
            const products = response?.data?.data?.reportTransaksiDone;
            setDataTransaksi(products);
          }
        });
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  const handleModal = async (transaksiID) => {
    setOpenTransaksi(true);
    setTransaksiId(transaksiID);
  };

  const handleSearch = (e) => {
    setSearch(e);
  };

  const ExportToCSV = () => {
    try {
      api({
        url: `api/v1/admin/report/excel_order`,
        method: "POST",
        responseType: "blob", // important
      }).then((response) => {
        const outputFilename = `Data transaksi.xls`;

        // If you want to download file automatically using link attribute.
        const url = URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", outputFilename);
        document.body.appendChild(link);
        link.click();
      });
    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    getAllInventory();
  }, [startDate, endDate, search]);

  return (
    <>
      <div>
        <div style={{ display: "flex", marginLeft: "10%" }}>
          <div style={{ display: "flex" }}>
            <label
              style={{ padding: "10px", display: "flex", alignItems: "center" }}
            >
              <b>Search</b>
            </label>
            <input
              type="text"
              style={{
                margin: "10px",
                backgroundColor: "#e8ebe1",
                borderRadius: "7rem",
              }}
              onChange={(e) => handleSearch(e.target.value)}
            />
          </div>
          <div style={{ display: "flex", height: "40px", marginTop: "10px" }}>
            <label
              style={{ padding: "10px", display: "flex", alignItems: "center" }}
            >
              <b>Number of rows</b>
            </label>
            <select
              style={{ backgroundColor: "#e8ebe1", borderRadius: ".7rem" }}
              // onChange={(e) => changeStatus(e.target.value)}
              // value={status}
            >
              <option value={"Waiting"}>10</option>
              <option value={"Ready"}>25</option>
              <option value={"Done"}>50</option>
            </select>
          </div>
          <div
            style={{
              width: "53%",
              display: "flex",
              justifyContent: "flex-end",
              marginRight: "3rem",
            }}
          >
            {/* <ExportToCSV dataTransaksi= {dataTransaksi}/> */}
            <button
              onClick={ExportToCSV}
              style={{
                width: "100px",
                height: "40px",
                marginTop: "10px",
                backgroundColor: "#9FB866",
                color: "white",
                cursor: "pointer",
              }}
            >
              Export
            </button>
          </div>
        </div>

        <div style={{ width: "100%", display: "flex" }}>
          <div className="whitescroll">
            <table className="table-report-product">
              <thead>
                <tr>
                  <th className="table-head row-left">No</th>
                  <th className="table-head">Name</th>
                  <th className="table-head">Nomor HP</th>
                  <th className="table-head">Tanggal</th>
                  <th className="table-head">Kurir</th>
                  {/* <th className='table-head'>Pic</th> */}
                  <th className="table-head">Action</th>
                </tr>
              </thead>

              <tbody>
                {dataTransaksi.map((e, index) => (
                  <tr key={index}>
                    <td className="table-body row-left">{index + 1}</td>
                    <td className="table-body">{e.nama_user}</td>
                    <td className="table-body">{e.no_handphone}</td>
                    <td className="table-body">{e.tgl_pengiriman}</td>
                    <td className="table-body">{e.metode_pengiriman}</td>
                    {/* <td className='table-body'>{e.picture}</td> */}
                    <td className="table-body">
                      <h4
                        onClick={() => handleModal(e.transaksiID)}
                        style={{ color: "#99BBED", cursor: "pointer" }}
                      >
                        Details
                      </h4>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            {dataTransaksi.length === 0 && (
              <PopupData width={"350px"} height={"250px"} />
            )}
          </div>
        </div>
        <div>
          {openTransaksi && (
            <ModalTransaksiReport
              TransaksiId={TransaksiId}
              handleModal={handleModal}
              dataTransaksi={dataTransaksi}
              getAllInventory={getAllInventory}
              closeModal={setOpenTransaksi}
            />
          )}
        </div>
        <PaginationProduct />
      </div>
    </>
  );
};

export default TableReportTransaksi;
