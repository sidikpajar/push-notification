import React from 'react'

const Index = () => {
  return (
    <div style={{marginLeft:"10%", display:"flex", fontSize:"13px"}}>
        <div>
            <h4>Pages:</h4>
        </div>
        <div style={{display:"flex", alignItems:"center", marginLeft:"20px"}}>
            <h3 style={{cursor:"pointer"}}>Prev 1</h3>
                <p>|</p>
                    <h3 style={{marginLeft:"5px", cursor:"pointer"}}>1</h3>
                    <h3 style={{marginLeft:"5px", cursor:"pointer"}}>2</h3>
                    <h3 style={{marginLeft:"5px", cursor:"pointer"}}>3</h3>
                    <h3 style={{marginLeft:"5px", cursor:"pointer"}}>4</h3>
                    <h3 style={{marginLeft:"5px", marginRight:"5px", cursor:"pointer"}}>5</h3>
                <p>|</p>
            <h3 style={{cursor:"pointer"}}>50 Next</h3>
        </div>
        <div style={{display:"flex", alignItems:"center", marginLeft:"20px"}}>
            <h4>Go to page</h4>
            <input type="number" style={{width:"40px", height:"8px", marginLeft:"10px", backgroundColor:"#e8ebe1"}}/>
        </div>
    </div>
  )
}

export default Index