/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react'
import api from 'api';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';

const Index = ({product, selectProduct}) => {
    const [file, setFile] = useState("");
    const [name, setName] = useState("");
    const [harga, setHarga] = useState("");
    const [jenisProduct, setJenisProduct] = useState("");
    const [isiProduct, setIsiProduct] = useState("");
    const [preview, setPreview] = useState("");
    const [errors, setErrors] = useState(true);
    const [isButton, setIsButton] = useState(true)
    const Router = useRouter()
    console.log('click',errors)

    useEffect(() => {
        getData()
        
    }, [product, errors])

    useEffect(() => {
        if (name?.length > 1 && harga?.length > 0 && jenisProduct?.length > 1 && isiProduct?.length > 1 && jenisProduct?.length > 1  ){
            setIsButton(false)
        } else {
            setIsButton(true)
        }
        
    }, [isButton, name, harga, jenisProduct, isiProduct])

    const getData = () => {
        setName(product.nama_product)
        setHarga(product.harga_product)
        setJenisProduct(product.jenis_product)
        setIsiProduct(product.isi_product)
        setPreview(product.product_image_url)
    }

   

    // const Validation = (value)=> {
    //     let error = {}
    //     setErrors(error)

    //     if(!value.name) {
    //         error.name = "Name Required"
    //     } else if (value.length < 5) {
    //         error.name = "Name must be more than 5 char"
    //     } else {}

    // }



    const onSubmit = (e) => {
        e.preventDefault();
        if (product == "") {
            const formData = new FormData();
            // const formData = new URLSearchParams();
            
            formData.append("productImage", file);
            formData.append("nama_product", name);
            formData.append("harga_product", harga);
            formData.append("jenis_product", jenisProduct);
            formData.append("isi_product", isiProduct);

            api.post(`api/v1/admin/add_product`, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then((response) => {
                console.log(response);
                Swal.fire({
                    icon: 'success',
                    title: 'Update Successfully!',
                    showConfirmButton: false,
                    timer: 1500
                  })
                  Router.reload()
   
            }).catch(error => {
                console.log(error);
            });

        } else {
            const formData = new URLSearchParams();
        
            formData.append("productImage", file);
            formData.append("nama_product", name);
            formData.append("harga_product", harga);
            formData.append("jenis_product", jenisProduct);
            formData.append("isi_product", isiProduct);


            api.put(`api/v1/admin/product/${product.productID}`, formData, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(() => {
                //console.log(response);
                Swal.fire({
                    icon: 'success',
                    title: 'Update Successfully!',
                    showConfirmButton: false,
                    timer: 1500
                  })
                  Router.reload()
            }).catch(error => {
                console.log(error);
            });
        }
    }

    const loadImage = (e) => {
        const image = e.target.files[0];
        setFile(image);
        if(e.target.files.length !== 0){
            setPreview(URL.createObjectURL(image));
        } else {
            console.log("Not found")
        }
    };

    const clickDelete =()=> {
        setPreview('')
        setFile('')
    }

    return (
        <div >
            <div className="inner-container-25">
                <form onSubmit={onSubmit} style={{margin:"-10px", marginBottom:"10px"}}>
                    <div style={{display:"grid", paddingLeft:"2px"}}>
                        {preview ? (
                            <>  
                                <div style={{width:"100%", height:"250px",}} >
                                    <img style={{width:"100%", height:"100%" , objectFit:"scale-down"}} src={preview}/>
                                </div>
                            </>
                        ) : (

                            ""
                            
                        )}
                        <div style={{display:"flex"}}>
                            <input
                                type="file"
                                className="form-input"
                                onChange={loadImage}
                                style={{ backgroundColor:"#E5E5E5", width:"47%"}}/>
                                {/* #E5E5E5 */}
                                <h2 onClick={clickDelete} style={{margin:"10px", cursor:"pointer", color:"red"}}>x</h2>
                        </div>
                    </div>
                </form>

                <form onSubmit={onSubmit} className="order-form" style={{ margin: "5px" }} id="form-input-order">
                    <div>
                        <label>Nama Product</label>
                        <input
                            type="text"
                            className="form-input"
                            value={name}
                            required={true}
                            onChange={(e) => setName(e.target.value)} />
                        
                        {errors && name?.length <= 0?<p style={{color:"red"}}>Name is required</p> : ""}
                    </div>
                    <div style={{marginTop:"10px"}}>
                        <label>Harga Product</label>
                        <input
                            type="number"
                            className="form-input"
                            value={harga}
                            onChange={(e) => setHarga(e.target.value)}
                            min={0}/>
                            {errors && harga?.length <= 0?<p style={{color:"red"}}>Harga is required</p> : ""}
                    </div>
                   
                    <div  style={{marginTop:"10px"}}>
                        <label>Jenis Product</label>
                        <input
                            type="text"
                            className="form-input"
                            value={jenisProduct}
                            onChange={(e) => setJenisProduct(e.target.value)}
                            
                        />
                        {errors && jenisProduct?.length <= 0?<p style={{color:"red"}}>Jenis product is required</p> : ""}
                    </div>
                    
                    <div  style={{marginTop:"10px"}}>
                        <label>Isi Product</label>
                        <input
                            type="text"
                            className="form-input"
                            value={isiProduct}
                            onChange={(e) => setIsiProduct(e.target.value)}
                        />
                        {errors && isiProduct?.length <= 0?<p style={{color:"red"}}>Isi product is required</p> : ""}
                    </div>
                </form>
            </div>
            <div className="submit-button-footer">
                <button
                disabled={isButton}
                onClick={onSubmit}
                    className="submit-button"
                >
                    Create Order
                </button>
            </div>
        </div>
    )
}

export default Index