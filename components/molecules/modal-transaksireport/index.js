import React, { useEffect, useState } from "react";
import api from "api";
import Loading from "../../../libs/Loading";

const Index = ({ closeModal, TransaksiId }) => {
  const [data, setData] = useState([]);
  // eslint-disable-next-line no-unused-vars
  const [loading, setLoading] = useState(false);

  const getAllInventory = () => {
    try {
      setLoading(true);
      api
        .get(`api/v1/admin/report/detail_transaksi?transaksiID=${TransaksiId}`)
        .then((response) => {
          console.log(response);

          if (response.statusCode == 401) {
            console.log("Harus Relogin");
          } else {
            let products = response?.data?.data?.result;
            setData(products);
            setLoading(false);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllInventory();
  }, []);

  return (
    <div>
      <div className="modal">
        <div className="modal-main-edit-inventory">
          <div className="modal-main-row">
            <button onClick={() => closeModal(false)}>Close</button>
          </div>

          {data?.map((o, index) => (
            <div
              style={{ backgroundColor: "white" }}
              className="card-inventory"
              key={index}
            >
              <p
                style={{
                  fontSize: "17px",
                  marginLeft: "20px",
                  paddingTop: "5px",
                }}
              >
                [{o.nama_user}] - {o.transaction_code}
              </p>
              <button
                className={
                  o.status_pengiriman === "Ready"
                    ? "btn-list-order-ready"
                    : o.status_pengiriman === "Waiting"
                    ? "btn-list-order-waiting"
                    : o.status_pengiriman === "Done"
                    ? "btn-list-order-done"
                    : "some-class"
                }
                style={{
                  width: "75px",
                  marginLeft: "20px",
                  marginBottom: "15px",
                  fontSize: "10px",
                }}
              >
                {o.status_pengiriman}
              </button>

              <div style={{ display: "flex", fontSize: "5px" }}>
                <div style={{ alignItems: "flex-end", marginLeft: "20px" }}>
                  <p style={{ fontSize: "15px" }}>Pengiriman:</p>
                  <p style={{ fontSize: "18px" }}>
                    <b>{o.metode_pengiriman}</b>
                  </p>
                </div>
                <div
                  style={{
                    marginLeft: "20%",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                  }}
                >
                  <p style={{ fontSize: "15px" }}>Tanggal Pesan:</p>
                  <p style={{ fontSize: "18px", margin: "0px" }}>
                    <b>{o.tgl_pengiriman}</b>
                  </p>
                </div>
              </div>

              <div>
                <p
                  style={{
                    fontSize: "16px",
                    marginLeft: "20px",
                    marginTop: "1px",
                    marginBottom: "20px",
                  }}
                >
                  Notes:
                </p>
                <p
                  style={{
                    width: "300px",
                    fontSize: "15px",
                    marginLeft: "20px",
                    marginTop: "-10px",
                  }}
                >
                  <b>{o.note}</b>
                </p>
              </div>
              <hr />
              <div style={{ marginLeft: "20px", fontSize: "16px" }}>
                {o.products.map((p, index) => {
                  if (p.product_packageID === null) {
                    return (
                      <p key={index}>
                        {p.nama_product} x {p.quantity}
                      </p>
                    );
                  }
                  if (typeof o.products[index - 1] != "undefined") {
                    if (
                      o.products[index].product_packageID ===
                      o.products[index - 1].product_packageID
                    ) {
                      return (
                        <li key={p.detail_transaksiID}>
                          {p.nama_product} x {p.quantity}
                        </li>
                      );
                    } else {
                      return (
                        <div key={index}>
                          <p>------------------------</p>
                          {p.nama_product_package}
                          <li key={p.detail_transaksiID}>
                            {p.nama_product} x {p.quantity}
                          </li>
                        </div>
                      );
                    }
                  } else {
                    return (
                      <div key={index}>
                        {p.nama_product_package}

                        <li key={p.detail_transaksiID}>
                          {p.nama_product} x {p.quantity}
                        </li>
                      </div>
                    );
                  }
                })}
              </div>
              {/* BUTTON */}
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  padding: "10px",
                  marginTop: "auto",
                }}
              >
                {/* <button 
                                    className={
                                        o.status_pengiriman === "Ready"
                                        ? "button-dones"
                                        : "button-done-disabled"}
                                    onClick={()=> updateOrderQueue(event, o)}
                                    
                                    >Done</button> */}
              </div>
            </div>
          ))}
          {data.length === 0 && <Loading />}
        </div>
      </div>
    </div>
  );
};

export default Index;
