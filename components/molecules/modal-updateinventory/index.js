import React, { useEffect, useState } from "react";
import api from "api";
import Swal from "sweetalert2";

const Index = ({ closeModal }) => {
  const [inventory, setInventory] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      api.post(`api/v1/admin/inventory`, {
        listInventory: inventory,
      });
      Swal.fire({
        icon: "success",
        title: "Update Successfully!",
        showConfirmButton: false,
        timer: 1500,
      });
      // Router.reload();
    } catch (error) {
      console.log(error);
    }
  };

  const getAllInventory = () => {
    try {
      api
        .get(`api/v1/admin/inventory`)
        .then((response) => {
          // console.log('msg', response);

          let products = response?.data?.data?.result;
          setInventory(products);
          if (response.statusCode == 401) {
            console.log('error entut')
          } else {
            let products = response?.data?.data?.listInventory;
            setInventory(products);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  const updateFieldChanged = (jumlah, index) => {
    let temp = inventory;
    const params = inventory[index].inventoryID;
    for (let i = 0; i < temp.length; i++) {
      if (temp[i].inventoryID === params) {
        temp[i].jumlah = jumlah;
      }
    }

    return [...temp];
  };

  useEffect(() => {
    getAllInventory();
  }, []);

  return (
    <div>
      <div className="modal">
        <div className="modal-main-edit-inventory">
          <div className="modal-main-row">
            <button onClick={() => closeModal(false)}>Close</button>
          </div>

          <div>
            <form className="product-form" onSubmit={handleSubmit}>
              {inventory?.map((o, index) => (
                <div
                  style={{
                    display: "flex",
                    columnGap: "10px",
                    marginBottom: "10px",
                    justifyContent: "center",
                  }}
                  key={index}
                >
                  <input
                    type="text"
                    className="form-input-inventory1"
                    name="nama_product"
                    value={o.nama_inventory}
                  />

                  <input
                    type="number"
                    className="form-input-inventory2"
                    name="jumlah"
                    value={o.jumlah}
                    onChange={(e) => {
                      if (e.target.value >= 0)
                        setInventory(updateFieldChanged(e.target.value, index));
                    }}
                    step="any"
                  />
                  <input
                    type="text"
                    className="form-input-inventory2"
                    name="satuan"
                    value={o.satuan}
                  />
                </div>
              ))}
              <div className="submit-button-footer">
                <button className="submit-button" type="submit">
                  Submit / Update
                </button>
                {/* disabled={!this.state.formValid} onClick={this.handleSubmit} */}
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Index;