import React from 'react'

const Index = (props) => {
  const {width, height} = props

  return (
    <React.Fragment>
      <div style={{ display: "flex", flexDirection: "column", justifyContent: "center" }}>
        <div style={{ display: "flex", flexDirection: "column", alignItems: "center", padding:"20px" , backgroundColor:"white", borderRadius:"1rem" }}>
          <img style={{ width: `${width}`, height: `${height}` }} src='/notfound.jpg' />
          <h3 style={{ fontSize: "25px", color: "green" }}>Data not found</h3>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Index