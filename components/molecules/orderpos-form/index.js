import api from "api";
import React from "react";
import { addOrder } from "../../../libs/order";
import ModalWindow from "../modal";
import Swal from "sweetalert2";
import logo from "/public/delete.png";
import Image from "next/image";

class OrderPosForm extends React.Component {
  constructor(props) {
    super(props);
    this.cartQueueModal = React.createRef();
    this.orderStatusSuccessModal = React.createRef();
    this.orderStatusFailedModal = React.createRef();
    this.state = {
      selectedCartQueueProduct: {
        product_packageID: "",
        nama_product_package: "",
        product_package_details: [],
      },
      default_product_package_details: [],
      // Form Data
      nama_user: "",
      no_handphone: "",
      jmlh_pengiriman: "",
      tgl_pengiriman: "",
      metode_pengiriman: "",
      tipe_order: "",
      deliveryID: "",
      products: [],
      listProductPackage: [],
      cartQueue: [],
      formErrors: {
        nama_user: "",
        no_handphone: "",
        jmlh_pengiriman: "",
        tgl_pengiriman: "",
        metode_pengiriman: "",
        tipe_order: "",
        cartQueue: "",
      },
      namaUserValid: false,
      noHandphoneValid: false,
      jmlhPengirimanValid: false,
      tglPengirimanValid: false,
      metodePengirimanValid: false,
      tipeOrderValid: false,
      cartQueueValid: false,
      formValid: false,
      // Form
      daftar_metode_pengiriman: [],
      daftar_tipe_order: [],
      _daftar_produk: [],
      daftar_produk: [],
      _daftar_produk_paket: [],
    };
  }

  deleteProductFromCart(index) {
    console.log("delete product", index);
  }

  deleteProductPackageFromCart(index) {
    console.log("delete product package", index);
  }

  addProductToCart(p) {
    if (p.productID != null) {
      for (let i = 0; i < this.state.products.length; i++) {
        if (this.state.products[i]["productID"] == p.productID) {
          let products = this.state.products.slice();
          products[i]["production_quantity"]++;
          this.setState({ products: products });
          return;
        }
      }

      let product = {
        productID: p.productID,
        nama_product: p.nama_product,
        harga_product: p.harga_product,
        jenis_product: p.jenis_product,
        isi_product: p.isi_product,
        product_image_url: p.product_image_url,
        production_quantity: 1,
      };

      this.state.products.push(product);
    }

    if (p.product_packageID != null) {
      for (let i = 0; i < this.state.listProductPackage.length; i++) {
        if (
          this.state.listProductPackage[i]["product_packageID"] ==
          p.product_packageID
        ) {
          let listProductPackage = this.state.listProductPackage.slice();
          for (
            let j = 0;
            j < listProductPackage[i]["product_package_details"].length;
            j++
          ) {
            listProductPackage[i]["product_package_details"][j][
              "production_quantity"
            ]++;
          }
          listProductPackage[i]["production_quantity"]++;
          listProductPackage[i]["is_costum"] = false;
          this.setState({ listProductPackage: listProductPackage });
          return;
        }
      }

      let productPackage = {
        product_packageID: p.product_packageID,
        nama_product_package: p.nama_product_package,
        harga_product_package: p.harga_product_package,
        product_package_details: p.product_package_details,
        product_image_url: p.product_package_details[0].product_image_url,
        production_quantity: 1,
        is_costum: false,
      };

      let product_package_detail = {
        product_package_details: p.product_package_details,
      };

      for (
        let i = 0;
        i < productPackage["product_package_details"].length;
        i++
      ) {
        productPackage["product_package_details"][i]["production_quantity"] = 1;
      }

      for (
        let i = 0;
        i < product_package_detail["product_package_details"].length;
        i++
      ) {
        product_package_detail["product_package_details"][i][
          "production_quantity"
        ] = 1;
      }

      this.state.listProductPackage.push(productPackage);
      this.state.default_product_package_details.push(product_package_detail);
    }
  }

  getDeliveryMethod() {
    api
      .get(`api/v1/admin/courier`)
      .then((response) => {
        //console.log(response);
        let data = response.data;
        let CourierList = data.data.CourierList;
        let courier = CourierList.map((c) => (
          <option key={c.deliveryID} value={c.nama_delivery} id={c.deliveryID}>
            {c.nama_delivery}
          </option>
        ));
        console.log(courier);

        this.setState({ daftar_metode_pengiriman: courier });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getOrderType() {
    api
      .get(`api/v1/admin/order_type`)
      .then((response) => {
        let data = response.data;
        let orderTypeList = data.data.OrderType;
        let orderType = orderTypeList.map((o) => (
          <option
            key={o.order_type_id}
            value={o.order_type_name}
            id={o.order_type_id}
          >
            {o.order_type_name}
          </option>
        ));

        this.setState({ daftar_tipe_order: orderType });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleDeleteCart(index) {
    const { products, listProductPackage, cartQueue } = this.state;
    const cart = cartQueue.splice(index, 1);
    this.setState({ cart });

    products.splice(index, 1);
    this.setState({ products });

    listProductPackage.splice(index, 1);
    this.setState({ listProductPackage });
  }

  handleSelectProduct(p) {
    this.addProductToCart(p);
    let listCartQueue = this.state.products.concat(
      this.state.listProductPackage
    );

    let cartQueue = listCartQueue.map((c, index) => (
      <div className="cart-queue-row" key={index} value={c}>
        <div className="cart-queue-col" style={{ display: "flex" }}>
          <div className="cart-queue-image-container">
            <img src={c.product_image_url} alt="juice.png"></img>
          </div>
          <div className="cart-queue-detail-container">
            <p hidden={!c.productID}>
              <b>{c.nama_product}</b> x {c.production_quantity}
            </p>
            <p hidden={!c.productID}>{c.isi_product}</p>
            <p hidden={!c.product_packageID}>
              <b>{c.nama_product_package}</b> x {c.production_quantity}
            </p>
            <button
              className="button-hover-custom"
              type="button"
              hidden={!c.product_packageID}
              onClick={(event) => this.showCartQueueModal(event, c)}
            >
              Custom
            </button>
          </div>
          <div style={{ marginTop: "10px", marginRight: "10px" }}>
            <Image
              style={{ cursor: "pointer" }}
              width={30}
              height={30}
              src={logo}
              onClick={() => this.handleDeleteCart(index)}
            ></Image>
            {/* <button style={{marginLeft:"20px" , padding:"1px", margin:"10px", backgroundColor:"white", cursor:"pointer"}}><img src=""/></button> */}
          </div>
        </div>
      </div>
    ));

    this.setState({ cartQueue: cartQueue }, () => {
      this.validateField("cartQueue", this.state.cartQueue);
    });
  }

  handleSetProductList(productList) {
    let daftar_produk = productList.map((p) => (
      <option key={p.productID} value={p.productID}>
        {p.nama_product}
      </option>
    ));

    this.setState({
      daftar_produk: daftar_produk,
      _daftar_produk: productList,
    });
  }

  handleSetProductPackageList(productPackageList) {
    this.setState({ _daftar_produk_paket: productPackageList });
  }

  validateField(fieldName, value) {
    let formErrors = this.state.formErrors;
    let namaUserValid = this.state.namaUserValid;
    let noHandphoneValid = this.state.noHandphoneValid;
    let jmlhPengirimanValid = this.state.jmlhPengirimanValid;
    let tglPengirimanValid = this.state.tglPengirimanValid;
    let tipeOrderValid = this.state.tipeOrderValid;
    let metodePengirimanValid = this.state.metodePengirimanValid;
    let cartQueueValid = this.state.cartQueueValid;

    switch (fieldName) {
      case "nama_user":
        namaUserValid = value.length >= 3;
        formErrors.nama_user = namaUserValid ? "" : "Name is too short";
        break;
      case "no_handphone":
        noHandphoneValid = value.match(
          /^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/g
        );
        formErrors.no_handphone = noHandphoneValid
          ? ""
          : "Phone number is not valid";
        break;
      case "jmlh_pengiriman":
        jmlhPengirimanValid = value > 0;
        formErrors.jmlh_pengiriman = jmlhPengirimanValid
          ? ""
          : "Delivery amount must be greater than 0";
        break;
      // case "tgl_pengiriman":
      //   tglPengirimanValid =
      //     new Date(value).setUTCHours(0, 0, 0, 0) >=
      //     new Date().setUTCHours(0, 0, 0, 0);
      //   formErrors.tgl_pengiriman = tglPengirimanValid
      //     ? ""
      //     : "Delivery date must be greater than or equal to current date";
      //   break;
      case "metode_pengiriman":
        metodePengirimanValid = value.length > 0;
        formErrors.metode_pengiriman = metodePengirimanValid
          ? ""
          : "Select a delivery method";
        break;
      case "tipe_order":
        tipeOrderValid = value.length > 0;
        formErrors.tipe_order = tipeOrderValid ? "" : "Select an order type";
        break;
      case "cartQueue":
        cartQueueValid = value.length > 0;
        formErrors.products = cartQueueValid ? "" : "Select a product";
        break;
      default:
        break;
    }

    this.setState(
      {
        formErrors: formErrors,
        namaUserValid: namaUserValid,
        noHandphoneValid: noHandphoneValid,
        jmlhPengirimanValid: jmlhPengirimanValid,
        tglPengirimanValid: tglPengirimanValid,
        metodePengirimanValid: metodePengirimanValid,
        tipeOrderValid: tipeOrderValid,
        cartQueueValid: cartQueueValid,
      },
      this.validateForm
    );
  }

  validateForm() {
    this.setState({
      formValid:
        this.state.namaUserValid &&
        this.state.noHandphoneValid &&
        this.state.jmlhPengirimanValid &&
        this.state.metodePengirimanValid &&
        this.state.cartQueueValid,
    });
  }

  addCustom = async (event) => {
    event.preventDefault();
    const { selectedCartQueueProduct, listProductPackage } = this.state;

    // let myObj =  JSON.stringify({selectedCartQueueProduct ,
    //                               angka : 300 * 200})

    // localStorage.setItem("custom_obj", myObj)

    // let myObjParse = JSON.parse(localStorage.getItem("custom_obj"));
    // console.log(myObjParse, 'custom_obj')

    // await listProductPackage.push(selectedCartQueueProduct)

    await listProductPackage.push(selectedCartQueueProduct);
    this.setState({ cart: listProductPackage });

    Swal.fire({
      icon: "success",
      title: "Update Successfully!",
      showConfirmButton: false,
      timer: 1500,
    });
    this.cartQueueModal.current.closeModal();

    // console.log('selectedqueue', selectedCartQueueProduct)
    // console.log('list', listProductPackage)
  };

  defaultCustom = (event, product_packageID) => {
    event.preventDefault();

    let default_product_package_details =
      this.state.default_product_package_details;

    for (let i = 0; i < default_product_package_details.length; i++) {
      if (
        default_product_package_details[i].product_package_details[0]
          .product_packageID == product_packageID
      ) {
        let details = JSON.parse(
          JSON.stringify(
            default_product_package_details[i].product_package_details
          )
        );

        let cartQueueProduct = {
          product_packageID:
            this.state.selectedCartQueueProduct.product_packageID,
          nama_product_package:
            this.state.selectedCartQueueProduct.nama_product_package,
          product_package_details: details,
        };

        let listProductPackage = this.state.listProductPackage;

        let details2 = JSON.parse(
          JSON.stringify(
            default_product_package_details[i].product_package_details
          )
        );

        for (let j = 0; j < listProductPackage.length; j++) {
          if (listProductPackage[j].product_packageID == product_packageID) {
            listProductPackage[j].product_package_details = details2;
          }
        }

        this.setState({
          selectedCartQueueProduct: cartQueueProduct,
          listProductPackage: listProductPackage,
        });
      }
    }
  };

  removeCustom = (event, index) => {
    event.preventDefault();

    let listProductPackage = this.state.listProductPackage;
    let selectedCartQueueProduct = this.state.selectedCartQueueProduct;

    selectedCartQueueProduct.product_package_details.splice(index, 1);

    for (let i = 0; i < listProductPackage.length; i++) {
      if (
        listProductPackage[i].product_packageID ==
        selectedCartQueueProduct.product_packageID
      ) {
        listProductPackage[i].product_package_details.splice(index, 1);
      }
    }

    this.setState({
      selectedCartQueueProduct: selectedCartQueueProduct,
      listProductPackage: listProductPackage,
    });
  };

  handleCustomProductChange = (event, p) => {
    event.preventDefault();

    let _daftar_produk = this.state._daftar_produk.slice();

    for (let i = 0; i < _daftar_produk.length; i++) {
      if (_daftar_produk[i].productID == event.target.value) {
        let product = {
          adminID: p.adminID,
          harga_product: _daftar_produk[i].harga_product,
          isi_product: _daftar_produk[i].isi_product,
          jenis_product: _daftar_produk[i].jenis_product,
          nama_product: _daftar_produk[i].nama_product,
          productID: _daftar_produk[i].productID,
          product_image_url: _daftar_produk[i].product_image_url,
          product_packageID: p.product_packageID,
          product_package_detailID: p.product_package_detailID,
          productcategoryID: p.productcategoryID,
          productimageID: p.productimageID,
          production_quantity: p.production_quantity,
          is_costum: true,
        };

        let selectedCartQueueProduct = this.state.selectedCartQueueProduct;

        for (
          let j = 0;
          j < selectedCartQueueProduct.product_package_details.length;
          j++
        ) {
          if (
            selectedCartQueueProduct.product_package_details[j]
              .product_package_detailID == product["product_package_detailID"]
          ) {
            selectedCartQueueProduct.product_package_details[j] = product;
            this.setState({
              selectedCartQueueProduct: selectedCartQueueProduct,
            });

            break;
          }
        }

        break;
      }
    }
  };

  handleCustomQuantityChange = (event, p) => {
    event.preventDefault();

    p.production_quantity = event.target.value;
  };

  handleChange = (event) => {
    event.preventDefault();
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  handleChangeDelivery = (event) => {
    event.preventDefault();

    let name = event.target.name;
    let value = event.target.value;

    const index = event.target.selectedIndex;
    const el = event.target.childNodes[index];
    const deliveryID = el.getAttribute("id");

    this.setState({ [name]: value, deliveryID: deliveryID }, () => {
      this.validateField(name, value);
    });
  };

  handleChangeOrderType = (event) => {
    event.preventDefault();

    let name = event.target.name;
    let value = event.target.value;

    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    let {
      nama_user,
      no_handphone,
      jmlh_pengiriman,
      tgl_pengiriman,
      deliveryID,
      metode_pengiriman,
      tipe_order,
      products,
      listProductPackage,
      note,
    } = this.state;

    await addOrder(
      nama_user,
      no_handphone,
      jmlh_pengiriman,
      tgl_pengiriman,
      deliveryID,
      metode_pengiriman,
      tipe_order,
      products,
      listProductPackage,
      note
    );

    this.setState({
      selectedCartQueueProduct: {
        product_packageID: "",
        nama_product_package: "",
        product_package_details: [],
      },
      default_product_package_details: [],
      // Form Data
      nama_user: "",
      no_handphone: "",
      jmlh_pengiriman: "",
      tgl_pengiriman: "",
      deliveryID: "",
      metode_pengiriman: "",
      tipe_order: "",
      note: "",
      products: [],
      listProductPackage: [],
      cartQueue: [],
      formErrors: {
        nama_user: "",
        no_handphone: "",
        jmlh_pengiriman: "",
        tgl_pengiriman: "",
        metode_pengiriman: "",
        tipe_order: "",
        cartQueue: "",
      },
      namaUserValid: false,
      noHandphoneValid: false,
      jmlhPengirimanValid: false,
      tglPengirimanValid: false,
      metodePengirimanValid: false,
      tipeOrderValid: false,
      cartQueueValid: false,
      formValid: false,
    });

    this.orderStatusSuccessModal.current.showModal();
  };

  toggleStatusModal = (type, status) => {
    if (type === "SUCCESS") {
      if (status) {
        this.orderStatusSuccessModal.current.showModal();
      } else {
        this.orderStatusSuccessModal.current.closeModal();
      }
    } else {
      if (status) {
        this.orderStatusFailedModal.current.showModal();
      } else {
        this.orderStatusFailedModal.current.closeModal();
      }
    }
  };

  showCartQueueModal = (event, c) => {
    event.preventDefault();

    this.cartQueueModal.current.showModal();

    let cartQueueProduct = {
      product_packageID: JSON.parse(JSON.stringify(c.product_packageID)),
      nama_product_package: JSON.parse(JSON.stringify(c.nama_product_package)),
      product_package_details: JSON.parse(
        JSON.stringify(c.product_package_details)
      ),
    };

    this.setState({ selectedCartQueueProduct: cartQueueProduct });
  };

  componentDidMount() {
    this.getDeliveryMethod();
    this.getOrderType();
  }

  cancelReset = () => {
    // event.preventDefault();

    this.setState({
      nama_user: "",
      no_handphone: "",
      jmlh_pengiriman: "",
      tgl_pengiriman: "",
      metode_pengiriman: "",
      cartQueue: "",
      note: "",
      products: [],
      listProductPackage: [],
    });
  };

  render() {
    // console.log('CARTQUEUE', this.state.listProductPackage)
    return (
      <div className="container-25">
        <ModalWindow ref={this.cartQueueModal}>
          <div className="cart-queue-modal-container-row">
            <label>
              <b>{this.state.selectedCartQueueProduct.nama_product_package}</b>
            </label>
          </div>

          {this.state.listProductPackage.map((e, index) => (
            <div key={index}>
              {e.product_package_details.map((p, index) => (
                <div
                  style={{
                    display: "flex",
                    margin: "2px",
                    marginLeft: "2rem",
                    marginRight: "2rem",
                  }}
                  key={index}
                >
                  <select
                    style={{
                      margin: "0.2rem",
                      width: "1000px",
                      borderRadius: ".4rem",
                    }}
                    defaultValue={p.productID}
                    onChange={(event) =>
                      this.handleCustomProductChange(event, p)
                    }
                  >
                    <option value={p.productID}>{p.nama_product}</option>
                    {this.state.daftar_produk}
                  </select>
                  <input
                    style={{
                      margin: "0.2rem",
                      width: "50px",
                      borderRadius: ".5rem",
                    }}
                    type="number"
                    onChange={(event) =>
                      this.handleCustomQuantityChange(event, p)
                    }
                    defaultValue={p.production_quantity}
                  ></input>
                  <button
                    style={{
                      margin: "0.2rem",
                      borderRadius: ".3rem",
                      width: "80px",
                    }}
                    onClick={(event) => this.removeCustom(event, index)}
                  >
                    X
                  </button>
                </div>
              ))}
            </div>
          ))}

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              margin: "1.5rem",
            }}
          >
            <button
              onClick={(event) =>
                this.defaultCustom(
                  event,
                  this.state.selectedCartQueueProduct.product_packageID
                )
              }
            >
              Default
            </button>
            <button style={{ marginLeft: "0.5rem" }} onClick={this.addCustom}>
              Simpan
            </button>
          </div>
          {/* {this.state.selectedCartQueueProduct.product_package_details.map(
            (p, index) => (
              <div className="cart-queue-modal-container-row" key={index}>
                <select
                  defaultValue={p.productID}
                  onChange={(event) => this.handleCustomProductChange(event, p)}
                >
                  <option value={p.productID}>{p.nama_product}</option>
                  {this.state.daftar_produk}
                </select>
                <input
                  type="number"
                  onChange={(event) =>
                    this.handleCustomQuantityChange(event, p)
                  }
                  defaultValue={p.production_quantity}
                ></input>
                <button onClick={(event) => this.removeCustom(event, index)}>
                  X
                </button>
              </div>
            )
          )} */}
        </ModalWindow>

        <ModalWindow ref={this.orderStatusSuccessModal}>
          <div className="cart-queue-modal-container-row">
            <label>
              <b>ORDER SUCCESS</b>
            </label>
          </div>
        </ModalWindow>

        <ModalWindow ref={this.orderStatusFailedModal}>
          <div className="cart-queue-modal-container-row">
            <label>
              <b>ORDER FAILED</b>
            </label>
          </div>
          <div>
            <button
              onClick={() => this.toggleStatusModal("FAILED", false)}
            >
              OK
            </button>
          </div>
        </ModalWindow>

        <div className="inner-container-25">
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <button
              style={{
                color: "red",
                backgroundColor: "rgba(244, 244, 244, 1)",
                cursor: "pointer",
              }}
              onClick={this.cancelReset}
            >
              Reset
            </button>
          </div>
          <div>
            <label>Pembeli</label>
          </div>
          <form
            className="order-form"
            onSubmit={this.handleSubmit}
            id="form-input-order"
          >
            <div>
              <label>Nama</label>
            </div>
            <div>
              <input
                type="text"
                className="form-input"
                name="nama_user"
                value={this.state.nama_user}
                onChange={this.handleChange}
              />
            </div>
            <div>
              <small>{this.state.formErrors.nama_user}</small>
            </div>
            <div>
              <label>Handphone / WA</label>
            </div>
            <div>
              <input
                type="tel"
                className="form-input"
                name="no_handphone"
                value={this.state.no_handphone}
                onChange={this.handleChange}
              />
            </div>
            <div>
              <small>{this.state.formErrors.no_handphone}</small>
            </div>
            <div>
              <label>Jumlah Pengiriman</label>
            </div>
            <div>
              <input
                type="number"
                className="form-input"
                name="jmlh_pengiriman"
                value={this.state.jmlh_pengiriman}
                onChange={this.handleChange}
              />
            </div>
            <div>
              <small>{this.state.formErrors.jmlh_pengiriman}</small>
            </div>
            <div>
              <label>Tanggal Pengiriman</label>
            </div>
            <div>
              <input
                type="date"
                className="form-input"
                name="tgl_pengiriman"
                value={this.state.tgl_pengiriman}
                onChange={this.handleChange}
              />
            </div>
            <div>
              <small>{this.state.formErrors.tgl_pengiriman}</small>
            </div>
            <div>
              <label>Metode Pengiriman</label>
            </div>
            <div>
              <select
                name="metode_pengiriman"
                className="form-input"
                onChange={this.handleChangeDelivery}
                value={this.state.metode_pengiriman}
              >
                <option value="">Pilih Pengiriman</option>
                {this.state.daftar_metode_pengiriman}
              </select>
            </div>
            <div>
              <small>{this.state.formErrors.metode_pengiriman}</small>
            </div>
            <div>
              <label>Tipe Order</label>
            </div>
            <div>
              <select
                name="tipe_order"
                className="form-input"
                onChange={this.handleChangeOrderType}
                value={this.state.tipe_order}
              >
                <option value="">Pilih Tipe Order</option>
                {this.state.daftar_tipe_order}
              </select>
            </div>
            <div>
              <small>{this.state.formErrors.tipe_order}</small>
            </div>

            <div>
              <small></small>
            </div>
            <div>
              <label>Note</label>
            </div>
            <div>
              <input
                type="text"
                className="form-input"
                name="note"
                value={this.state.note}
                onChange={this.handleChange}
              />
            </div>

            <div>
              <small>{this.state.formErrors.metode_pengiriman}</small>
            </div>
          </form>
          <div>
            <label>Keranjang Belanja</label>
          </div>
          <small>{this.state.formErrors.products}</small>
          <div className="cart-item-list">{this.state.cartQueue}</div>
        </div>
        <div className="submit-button-footer">
          <button
            className="submit-button"
            disabled={!this.state.formValid}
            onClick={this.handleSubmit}
          >
            Create Order
          </button>
        </div>
      </div>
    );
  }
}

export default OrderPosForm;
