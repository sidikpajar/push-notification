/* eslint-disable no-unused-vars */
import React from "react";
import Services from "./services";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie, getElementAtEvent } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { useRef } from "react";
// import "react-date-range/dist/styles.css"; // main style file
// import "react-date-range/dist/theme/default.css"; // theme css file
// import { DateRangePicker } from "react-date-range";
import moment from "moment";

ChartJS.register(ArcElement, Tooltip, Legend);

const options = {
  plugins: {
    datalabels: {
      display: false, // Menyembunyikan label
    },
  },
};

const ChartTransaksi = () => {
  const chartRef = useRef();
  const { data, handleSelect, date, show, setShow } = Services();
  const onClick = (event) => {
    console.log(getElementAtEvent(chartRef.current, event));
  };

  if (data !== undefined)
    return (
      <div
        style={{
          borderWidth: 1,
          borderRadius: "10px",
          borderColor: "black",
          boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
          padding: "16px",
        }}
      >
        {show && (
          <div className="modal">
            <div className="modal-main-edit-calendar">
              <div className="modal-main-row">
                <button onClick={() => setShow(false)}>Close</button>
              </div>

              <div className="">
                {/* <DateRangePicker
                  onChange={handleSelect}
                  showSelectionPreview={true}
                  moveRangeOnFirstSelection={false}
                  months={2}
                  ranges={date}
                  value={date}
                  direction="horizontal"
                /> */}
              </div>
            </div>
          </div>
        )}

        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>

          <h2>Transkasi</h2>
          <div onClick={() => setShow(true)}>
            {moment(date?.[0].startDate).format("DD/MM/YYYY")} - {moment(date?.[0].endDate).format("DD/MM/YYYY")}
          </div>

        </div>
        <Pie
          ref={chartRef}
          data={data}
          options={options}
          plugins={[ChartDataLabels]}
          onClick={onClick}
        />
        <div style={{ fontSize: 16 }}>
          <div>
            Terbuat: <b>{data?.datasets?.[0].data?.[0]}</b>
          </div>
          <div>
            Tercancel: <b>{data?.datasets?.[0].data?.[1]}</b>
          </div>
          <div>
            Dalam Antrian: <b>{data?.datasets?.[0].data?.[2]}</b>
          </div>
          <div>
            Total: <b>{data?.total}</b>
          </div>
        </div>
        <div style={{ textAlign: "right" }}>Lihat Detail</div>
      </div>
    );
};

export default ChartTransaksi;
