import React from "react";
import { PopupData } from "components/molecules";
import { StartEndDate } from "components/atom";
import ModalConfirmationCancel from "components/molecules/modal-confirmation-cancel";
import ServicesOrderList from "./services";

const Index = () => {
  const {
    HandleEndDate,
    HandleStartDate,
    cancelEnvirontment,
    changeStatus,
    onSubmit,
    orderLists,
    setCancelEnvirontment,
    moment,
    startDate,
    endDate,
    status
  } = ServicesOrderList()
  return (
    <div>
      <div style={{ display: "flex", maxWidth: "600px" }}>
        <StartEndDate
          HandleStartDate={HandleStartDate}
          HandleEndDate={HandleEndDate}
          startDate={startDate}
          color={"white"}
          marginLeft={"15px"}
          endDate={endDate}
        />
        <div style={{ marginTop: "13px" }}>
          <select
            style={{ margin: "24px", backgroundColor: "white" }}
            onChange={(e) => changeStatus(e.target.value)}
            value={status}
          >
            <option value={"All"}>All</option>
            <option value={"Waiting"}>Waiting</option>
            <option value={"Ready"}>Ready</option>
            <option value={"Done"}>Done</option>
            <option value={"Cancel"}>Cancel</option>
          </select>
        </div>
      </div>

      <div style={{ marginTop: "20px" }}>
        {orderLists?.length === 0 && (
          <PopupData width={"400px"} height={"300px"} />
        )}
      </div>
      <div className="container-inventory-col-92">
        <div className="container-product-inventory-92">
          {orderLists?.map((o, index) => (
            <div
              style={{
                backgroundColor: "#F4F4F4",
                padding: "0",
                position: "relative",
              }}
              className="card-inventory"
              key={index}
            >
              {/* textOverflow:"ellipsis", marginBottom:"15px", overflow:"hidden" */}
              {(o.status_pengiriman === "Ready" ||
                o.status_pengiriman === "Waiting") && (
                <button
                  onClick={() =>
                    setCancelEnvirontment({ id: o.transaksiID, show: true })
                  }
                  style={{
                    fontSize: "1.5em",
                    position: "absolute",
                    top: 0,
                    right: 0,
                    backgroundColor: "transparent",
                    cursor: "pointer",
                  }}
                >
                  X
                </button>
              )}
              <div
                style={{
                  width: "90%",
                  marginLeft: "20px",
                  marginBottom: "20px",
                }}
              >
                <p
                  style={{
                    fontSize: "14px",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                  }}
                >
                  [{o.nama_user}] - {o.transaction_code}
                </p>
              </div>
              <button
                className={
                  o.status_pengiriman === "Ready"
                    ? "btn-list-order-ready"
                    : o.status_pengiriman === "Waiting"
                    ? "btn-list-order-waiting"
                    : o.status_pengiriman === "Done"
                    ? "btn-list-order-done"
                    : o.status_pengiriman === "Cancel"
                    ? "btn-list-order-cancel"
                    : "some-class"
                }
                style={{
                  width: "75px",
                  marginLeft: "20px",
                  marginBottom: "15px",
                  fontSize: "10px",
                }}
              >
                {o.status_pengiriman}
              </button>
              {o?.status_pengiriman === "Cancel" && (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    marginLeft: "20px",
                  }}
                >
                  <span style={{marginBottom: '0.5em'}}>Canceled By :</span>
                  <span style={{marginBottom: '0.5em'}}>{o?.log_transaksi?.updated_by?.split("|")[1]}</span>
                  <span style={{marginBottom: '0.5em'}}>
                    Date:{" "}
                    {moment(o?.log_transaksi?.updated_at).format(
                      "DD/MM/YYYY, HH:mm"
                    )}
                  </span>
                </div>
              )}
              <div style={{ display: "flex", fontSize: "5px" }}>
                <div style={{ alignItems: "flex-end", marginLeft: "20px" }}>
                  <p style={{ fontSize: "14px" }}>Pengiriman:</p>
                  <p style={{ fontSize: "14px" }}>
                    <b>{o.metode_pengiriman}</b>
                  </p>
                </div>
                <div style={{ alignItems: "flex-end", marginLeft: "20px" }}>
                  <p style={{ fontSize: "14px" }}>Tipe Order:</p>
                  <p style={{ fontSize: "14px" }}>
                    <b>{o.tipe_order}</b>
                  </p>
                </div>
                <div
                  style={{
                    marginLeft: "20%",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                  }}
                >
                  <p style={{ fontSize: "14px" }}>Tanggal Pesan:</p>
                  <p style={{ fontSize: "13px", margin: "0px" }}>
                    <b>{o.tgl_pengiriman}</b>
                  </p>
                </div>
              </div>

              <div>
                <p
                  style={{
                    fontSize: "16px",
                    marginLeft: "20px",
                    marginTop: "1px",
                    marginBottom: "20px",
                  }}
                >
                  Notes:
                </p>
                <p
                  style={{
                    width: "300px",
                    fontSize: "15px",
                    marginLeft: "20px",
                    marginTop: "-10px",
                  }}
                >
                  {o.note}
                </p>
              </div>
              <hr />
              <div style={{ marginLeft: "20px", fontSize: "16px" }}>
                {o.products.map((p, index) => {
                  if (p.product_packageID === null) {
                    return (
                      <p key={index}>
                        {p.nama_product} x {p.quantity}
                      </p>
                    );
                  }
                  if (typeof o.products[index - 1] != "undefined") {
                    if (
                      o.products[index].product_packageID ===
                      o.products[index - 1].product_packageID
                    ) {
                      return (
                        <li key={p.detail_transaksiID}>
                          {p.nama_product} x {p.quantity}
                        </li>
                      );
                    } else {
                      return (
                        <div key={index}>
                          <p>------------------------</p>
                          {p.nama_product_package}
                          <li key={p.detail_transaksiID}>
                            {p.nama_product} x {p.quantity}
                          </li>
                        </div>
                      );
                    }
                  } else {
                    return (
                      <div key={index}>
                        {p.nama_product_package}

                        <li key={p.detail_transaksiID}>
                          {p.nama_product} x {p.quantity}
                        </li>
                      </div>
                    );
                  }
                })}
              </div>
              {/* BUTTON */}
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  padding: "10px",
                  marginTop: "auto",
                }}
              >
            </div>
              </div>
          ))}
        </div>
      </div>
      {cancelEnvirontment?.show && (
        <ModalConfirmationCancel
          onAbort={() => setCancelEnvirontment({ id: null, show: false })}
          onConfirm={onSubmit}
        />
      )}
    </div>
  );
};

export default Index;
