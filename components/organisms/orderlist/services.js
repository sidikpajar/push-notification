import { useState, useEffect } from "react";
import moment from "moment";
import Swal from "sweetalert2";
import api from "api";

const ServicesOrderList = ( ) => {
  const [orderLists, setOrderList] = useState([]);
  const [status, setStatus] = useState(null);
  const [cancelEnvirontment, setCancelEnvirontment] = useState({
    show: false,
    id: null,
  });
  const [startDate, setStartDate] = useState(
    moment(new Date()).subtract(7, "day").format("YYYY-MM-DD")
  );
  const [endDate, setEndDate] = useState(
    moment(new Date()).format("YYYY-MM-DD")
  );
  
  const getData = () => {
    let url = "";
    if (status) {
      url = `api/v1/admin/order_all?date_from=${startDate}&date_end=${endDate}&status_pengiriman=${status}`;
    } else {
      url = `api/v1/admin/order_all?date_from=${startDate}&date_end=${endDate}`;
    }

    try {
      api.get(url).then((response) => {
        if (response.statusCode == 401) {
          console.log('harus login')
        } else {
          let products = response?.data?.data?.result;
          setOrderList(products);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = () => {
    api.delete(`api/v1/admin/order_product/${cancelEnvirontment?.id}`)
    .then(() => {
      Swal.fire({
        icon: "success",
        title: "Order Berhasil Dicancel",
        showConfirmButton: false,
        timer: 1500,
        willClose: () => window.location.reload()
      });
    }).catch(() => {
      Swal.fire({
        icon: "error",
        title: "Order Gagal Dicancel",
        showConfirmButton: true,
      });
    })
  };

  const HandleStartDate = (startdate) => {
    setStartDate(startdate);
  };

  const HandleEndDate = (enddate) => {
    setEndDate(enddate);
  };

  const changeStatus = (status) => {
    if (status === "All") {
      setStatus(null);
    } else {
      setStatus(status);
    }
  };

  useEffect(() => {
    getData();
  }, [status, endDate, startDate]);

  return {
    changeStatus,
    HandleEndDate,
    HandleStartDate,
    onSubmit,
    cancelEnvirontment,
    setCancelEnvirontment,
    orderLists,
    moment,
    startDate,
    endDate,
    status
  }
}

export default ServicesOrderList;