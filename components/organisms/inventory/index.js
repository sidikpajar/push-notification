import { ButtonBack } from 'components/atom';
import { ModalLog, ModalUpdateInventory, ProductInventory } from 'components/molecules'
import React, { useState } from 'react'

const Index = () => {
    const [openUpdate, setOpenUpdate] = useState(false)
    const [openLog, setOpenLog] = useState(false)

  return (
    <React.Fragment>
        <div style={{ backgroundColor: "white", height: "100vh" }}>
                <div>
                    <div style={{padding:"20px"}}>
                        <ButtonBack style={"back-button"} route={"/"}/>
                    </div>
                    <div style={{ marginLeft: "10px" }}>
                        <h5 style={{ fontSize: "19px", marginLeft: '15px' }}><b>Inventory</b></h5>

                        <div style={{
                            display: "flex",
                            justifyContent: "flex-end",
                            marginTop: "-70px",
                            // backgroundColor:"red",
                            maxWidth: "800px",
                            marginLeft: "40%"
                        }}>

                            <button
                                className="back-button"
                                onClick={() => {
                                    setOpenLog(true)
                                }}
                            >Log

                            </button>

                            <button
                                className="back-button"
                                style={{ width: "15%", marginRight: "170px" }}
                                onClick={() => {
                                    setOpenUpdate(true)
                                }}
                            >
                                Update

                            </button>
                        </div>
                    </div>
                    <div>
                        <ProductInventory />
                        {openUpdate && <ModalUpdateInventory closeModal={setOpenUpdate} />}
                        {openLog && <ModalLog closeModal={setOpenLog} />}
                    </div>
                </div>
            </div>
    </React.Fragment>
  )
}

export default Index