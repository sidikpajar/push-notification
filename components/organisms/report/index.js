import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import api from 'api'

const Index = ({ startDate, endDate }) => {
    const [dataTransaksi, setDataTransaksi] = useState([])
    const [dataProduct, setDataProduct] = useState([])
    const [dataIdProduct, setDataIdProduct] = useState([])
    const [dataIdTransaksi, setDataIdTransaksi] = useState([])


    const getDataTransaksi = () => {
        try {
            api.get(`api/v1/admin/report/transaksi?date_from=${startDate}&date_end=${endDate}`)
                .then((response) => {
                    //   console.log(response)
                    if (response.statusCode == 401) {
                        console.log('Harus relogin')
                    } else {
                        const products = (response?.data?.data?.reportTransaksiDone)
                        setDataTransaksi(products)
                    }
                })
        } catch (error) {
            console.log(error)
        }
    }

    const getDataProduct = () => {
        try {
            // &search=Car
            api.get(`api/v1/admin/report/product?date_from=${startDate}&date_end=${endDate}`)
                .then((response) => {
                    //   console.log(response)

                    if (response.statusCode == 401) {
                        console.log('Harus relogin')
                    } else {
                        const products = (response?.data?.data?.reportProductionDone)
                        setDataProduct(products)
                    }
                })
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getDataTransaksi()
        getDataProduct()
    }, [startDate, endDate])

    useEffect(() => {
        setDataIdTransaksi(dataTransaksi.length)
        setDataIdProduct(dataProduct.length)

    }, [dataTransaksi, dataProduct])


    return (
        <div className='container-inventory-col-92'>
            <div className='container-product-inventory' style={{ marginTop: "2rem", marginLeft: "2rem", marginRight: "2rem" }} >
                <div style={{ display: "grid", backgroundColor: "#F4F4F4", padding: "3rem", justifyContent: "center" }} className="card-inventory" >
                    <p style={{ textAlign: "center" }} className='title-nama-inventory'>Product Terbuat</p>
                    <p style={{ textAlign: "center", fontSize: "40px", marginTop: "-10px" }} className='title-nama-inventory'>{dataIdProduct}</p>
                    <Link href="/report/report-product">
                        <a>
                            <button style={{ fontSize: "20px", cursor: "pointer", marginTop: "-10px", width: "130px", marginLeft: "3%" }}>Detail</button>
                        </a>
                    </Link>
                </div>

                <div style={{ display: "grid", backgroundColor: "#F4F4F4", padding: "3rem", justifyContent: "center" }} className="card-inventory" >
                    <p style={{ textAlign: "center" }} className='title-nama-inventory'>Transaksi</p>
                    <p style={{ textAlign: "center", fontSize: "40px", marginTop: "-10px" }} className='title-nama-inventory'>{dataIdTransaksi}</p>
                    <Link href="/report/report-transaksi">
                        <a>
                            <button style={{ fontSize: "20px", cursor: "pointer", marginTop: "-10px", width: "130px" }}>Detail</button>
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Index