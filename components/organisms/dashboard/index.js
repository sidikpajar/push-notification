import api from "api";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import logo from "/public/logo-livera.png";
import { FaRegBell, FaRegHandPointRight } from "react-icons/fa";

const Index = () => {
  const router = useRouter();
  const logoutUser = () => {
    api
      .post(`api/v1/admin/logout`)
      .then((response) => {
        if(response){
          localStorage.clear();
          router.replace("/login")
        }
      })
      .catch((error) => {
        console.log(error);
      });

  }

  return (
    <div className="menu">
      <div className="logo-container">
        <Image src={logo} alt="logo.png" width={256} height={256}></Image>
        <br></br>
      </div>
      <div style={{display:"flex", justifyContent:"center"}}>
        <FaRegBell style={{margin:"20px", cursor:'pointer'}} size={30}/>
        <FaRegHandPointRight style={{margin:"20px", cursor:'pointer'}}  size={30}/>
      </div>
      <Link href="/orderpos">
        <button className="menu-button">Order Pos</button>
      </Link>
      <Link href="/orderlist/">
        <button className="menu-button">Order List</button>
      </Link>
      <Link href="/prodqueue">
        <button className="menu-button">Product Queue</button>
      </Link>
      <Link href="/product-setting/productsetting">
        <button className="menu-button">Product Setting</button>
      </Link>
      <Link href="/inventory/">
        <button className="menu-button">Inventory</button>
      </Link>
      <Link href="/report">
        <button className="menu-button" style={{ marginBottom: "50px" }}>Report</button>
      </Link>
      <button style={{ marginBottom: "50px" }} className="menu-button" onClick={logoutUser}>
        Logout
      </button>
    </div>
  );
}

export default Index