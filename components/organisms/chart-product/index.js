/* eslint-disable no-unused-vars */
import React from "react";
import Services from "./services";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie, getElementAtEvent } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { useRef } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import moment from "moment";

ChartJS.register(ArcElement, Tooltip, Legend);

const options = {
  plugins: {
    datalabels: {
      display: false, // Menyembunyikan label
    },
  },
};

const ChartProduct = () => {
  const chartRef = useRef();
  const { data, startDate, setStartDate, endDate, setEndDate } = Services();

  const onClick = (event) => {
    console.log(getElementAtEvent(chartRef.current, event));
  };

  if (data !== undefined)
    return (
      <div
        style={{
          borderWidth: 1,
          borderRadius: "10px",
          borderColor: "black",
          boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
          padding: "16px",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div style={{width: '50%'}}><h2>Produk</h2></div>
          
          <div onClick={() => console.log("")} style={{display: 'flex', width: '50%', flexDirection: "row", justifyContent: "space-between", alignItems: 'center'}}>
            <DatePicker
              selected={startDate}
              onChange={(date) => setStartDate(date)}
            />
            <DatePicker
              selected={endDate}
              onChange={(date) => setEndDate(date)}
            />
            {/*             
            {moment(date?.[0].startDate).format("DD/MM/YYYY")} -{" "}
            
            {moment(date?.[0].endDate).format("DD/MM/YYYY")} */}
          </div>
        </div>
        <Pie
          ref={chartRef}
          data={data}
          options={options}
          plugins={[ChartDataLabels]}
          onClick={onClick}
        />
        <div style={{ fontSize: 16 }}>
          <div>
            Terbuat: <b>{data?.datasets?.[0].data?.[0]}</b>
          </div>
          <div>
            Tercancel: <b>{data?.datasets?.[0].data?.[1]}</b>
          </div>
          <div>
            Dalam Antrian: <b>{data?.datasets?.[0].data?.[2]}</b>
          </div>
          <div>
            Total: <b>{data?.total}</b>
          </div>
        </div>
        <div style={{ textAlign: "right" }}>Lihat Detail</div>
      </div>
    );
};

export default ChartProduct;
