import { useState, useEffect } from "react";
import api from "api";
import moment from "moment";

const Services = ( ) => {
  const [data, setData] = useState();
  const [show, setShow] = useState(false);
  const [date, setDate] = useState([
    {
      startDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
      endDate: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
      key: 'selection',
    },
  ]);

  const handleSelect =(ranges) => {
    setDate([ranges.selection]);
  }

  useEffect(() => {
    fetchProduct();
  }, []);

  const fetchProduct = () => {
    api.get(`api/v1/admin/report/chart/paket?start_date=${moment(date?.[0].startDate).format("YYYY-MM-DD")}&end_date=${moment(date?.[0].endDate).format("YYYY-MM-DD")}`).then((response) => {
     
      const data = {
        labels: ['Terbuat', 'Tercancel', 'Dalam Antrian'],
        total: response?.data?.data?.total,
        datasets: [
          {
            label: 'Produk dipesan',
            data: [response?.data?.data?.terbuat, response?.data?.data?.tercancel, response?.data?.data?.dalam_antrian],
            backgroundColor: [
              'rgba(255, 206, 86, 0.2)',
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: [
              'rgba(255, 206, 86, 1)',
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1,
          },
        ],
      };
      setData(data)
    });
  }

  return {
    data,
    handleSelect,
    date,
    show, setShow
  }
}

export default Services;