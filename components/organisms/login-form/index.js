import Image from 'next/image'
import React from 'react'
import { loginUser } from '../../../libs/auth'
import logo from '../../../public/logo-livera.png'

class LoginForm extends React.Component {
    state = {
        email: '',
        password: '',
        formErrors: { email: '', password: ''},
        emailValid: false,
        passwordValid: false,
        formValid: false,
    };

    validateField(fieldName, value) {
        let formErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case "email":
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                formErrors.email = emailValid ? "" : "Email is invalid";
                break;
            case "password":
                passwordValid = value.length >= 6;
                formErrors.password = passwordValid ? "": "Password is too short";
                break;
            default:
                break;
        }

        this.setState({
            formErrors: formErrors,
            emailValid: emailValid,
            passwordValid: passwordValid}, this.validateForm)
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid})
    }

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value }, () => { this.validateField(name, value) });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        let { email, password } = this.state;

        loginUser(email, password);
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="logo-container">
                    <Image src={logo} alt="logo.png" width={256} height={256}></Image><br></br>
                </div>
                <div><label>EMAIL</label></div>
                <div>
                    <input
                    className="login-form"
                    type="email"
                    name="email"
                    onChange={this.handleChange}
                    />
                </div>
                <div>
                    <small>{this.state.formErrors.email}</small>
                </div>
                <div><label>PASSWORD</label></div>
                <div>
                    <input
                    className="login-form"
                    type="password"
                    name="password"
                    onChange={this.handleChange}
                    />
                </div>
                <div>
                    <small>{this.state.formErrors.password}</small>
                </div>
                <div>
                    <button className='button-login' type="submit" disabled={!this.state.formValid}>Login</button>
                </div>
            </form>
        )
    }
}

export default LoginForm;