import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import api from "api";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Pagination } from "swiper";

const SliderOrder = () => {
  const [orderLists, setOrderList] = useState([]);

  console.log(orderLists);

  // const getAllProduct = async() => {
  //     const res = await api.get(`${process.env.API_URL}api/v1/admin/product`)

  //     setProduct(products)
  // }

  const getData = () => {
    try {
      api.get(`api/v1/admin/order`).then((response) => {
        if (response.statusCode == 401) {
          console.log('harus login')
        } else {
          let products = response?.data?.data?.result;
          setOrderList(products);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  const updateOrderQueue = (event, o) => {
    event.preventDefault();

    api
      .put(`api/v1/admin/order_done/${o.transaksiID}`)
      .then((response) => {
        console.log(response);
        getData();
        console.log("sukses");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div className="container-slide">
        <Swiper
          slidesPerView={3}
          spaceBetween={30}
          centeredSlides={true}
          // clickable={true}
          modules={[Pagination]}
          className="mySwiper"
          breakpoints={{
            300: {
              spaceBetween: 30,
              slidesPerView: 1,
            },
            600: {
              spaceBetween: 30,
              slidesPerView: 2,
            },
            1000: {
              spaceBetween: 30,
              slidesPerView: 3,
            },
          }}
        >
          {orderLists?.map((o, index) => (
            <SwiperSlide key={index}>
              {/* btn-list-order */}

              <div className="card-orderlist">
                <p
                  style={{
                    fontSize: "18px",
                    marginLeft: "20px",
                    paddingTop: "20px",
                  }}
                >
                  [{o.nama_user}] - {o.transaction_code}
                </p>
                <button
                  className={
                    o.status_pengiriman === "Ready"
                      ? "btn-list-order-ready"
                      : "btn-list-order-waiting"
                  }
                  style={{
                    width: "100px",
                    marginLeft: "20px",
                    marginBottom: "15px",
                  }}
                >
                  {o.status_pengiriman}
                </button>

                <div style={{ display: "flex", fontSize: "5px" }}>
                  <div style={{ alignItems: "flex-end", marginLeft: "20px" }}>
                    <p style={{ fontSize: "15px" }}>Pengiriman:</p>
                    <p style={{ fontSize: "17px" }}>
                      <b>{o.metode_pengiriman}</b>
                    </p>
                  </div>
                  <div
                    style={{
                      marginLeft: "20%",
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "flex-start",
                    }}
                  >
                    <p style={{ fontSize: "15px" }}>Tanggal Pesan:</p>
                    <p style={{ fontSize: "17px" }}>
                      <b>{o.tgl_pengiriman}</b>
                    </p>
                  </div>
                </div>

                <div>
                  <p
                    style={{
                      fontSize: "16px",
                      marginLeft: "20px",
                      marginTop: "1px",
                      marginBottom: "20px",
                    }}
                  >
                    Notes:
                  </p>
                  <p
                    style={{
                      width: "300px",
                      fontSize: "18px",
                      marginLeft: "20px",
                      marginTop: "-10px",
                    }}
                  >
                    {o.note}

                    {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mi 
                               ulputate in. */}
                  </p>
                </div>
                <hr />
                <div style={{ marginLeft: "20px", fontSize: "16px" }}>
                  {o.products.map((p, index) => {
                    if (p.product_packageID === null) {
                      return (
                        <p key={index}>
                          {p.nama_product} x {p.quantity}
                        </p>
                      );
                    }
                    if (typeof o.products[index - 1] != "undefined") {
                      if (
                        o.products[index].product_packageID ===
                        o.products[index - 1].product_packageID
                      ) {
                        return (
                          <li key={p.detail_transaksiID}>
                            {p.nama_product} x {p.quantity}
                          </li>
                        );
                      } else {
                        return (
                          <div>
                            <p>------------------------</p>
                            {p.nama_product_package}
                            <li key={p.detail_transaksiID}>
                              {p.nama_product} x {p.quantity}
                            </li>
                          </div>
                        );
                      }
                    } else {
                      return (
                        <div>
                          {p.nama_product_package}

                          <li key={p.detail_transaksiID}>
                            {p.nama_product} x {p.quantity}
                          </li>
                        </div>
                      );
                    }
                  })}
                </div>
                {/* BUTTON */}
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    padding: "10px",
                  }}
                >
                  <button
                    className={
                      o.status_pengiriman === "Ready"
                        ? "button-dones"
                        : "button-done-disabled"
                    }
                    onClick={() => updateOrderQueue(event, o)}
                  >
                    Done
                  </button>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </>
  );
};

export default SliderOrder;
