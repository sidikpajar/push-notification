import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import jwtDecode from "jwt-decode";
import api from "api";
import firebase from 'firebase/compat/app';
import 'firebase/compat/database';

function Auth(props) {
  const [accessGranted, setAccessGranted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const publicRouter = ["/login"];

  const checkAuth = async () => {
    let accessToken = typeof window !== "undefined" ? window.localStorage.getItem("LOGIN_TOKEN") : null
    window.addEventListener("online", () => console.log("Became online"));
    setIsLoading(true)
    if (!window.navigator.onLine) {
      router.push("/no-connection");
    } else {
      if (accessToken && isAuthTokenValid(accessToken)) {
        // setSession(accessToken);
        api.defaults.headers.common["Authorization"] =`Bearer ${accessToken}`;
        setAccessGranted(true);
        if (publicRouter.includes(router.pathname)) {
           router.push(`/`);
        }
      } else {
        setAccessGranted(true);
        clearSession();
      }
    }
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  };

  const setInterceptors = () => {
    api.interceptors.response.use(
      (response) => {
        if (response.data.message === "Unauthorized") {
          clearSession();
        }

        // if (response.status === 404) {
        //   router.push("/error-system");
        // }

        return response;
      },
      (err) => {
        throw err;
      }
    );
  };

  const isAuthTokenValid = (accessToken) => {
    if (accessToken === "" || accessToken === undefined) {
      return false;
    }
    const decoded = jwtDecode(accessToken);
    const currentTime = Date.now() / 1000;
    if (decoded.exp > currentTime) {
      return true;
    } else {
      return false;
    }
  };


  const clearSession = () => {
    localStorage.clear()
    if (!publicRouter.includes(router.pathname)) {
      router.push("/login");
    }
  };

  useEffect(() => {
    setInterceptors();
    checkAuth();
  }, []);

  useEffect(() => {
    const firebaseConfig = {
      apiKey: "AIzaSyBkg8GyoekCDG4DE7Z_5lwq0qXWGYDulMQ",
      authDomain: "livera-d3044.firebaseapp.com",
      databaseURL: "https://livera-d3044-default-rtdb.asia-southeast1.firebasedatabase.app",
      projectId: "livera-d3044",
      storageBucket: "livera-d3044.appspot.com",
      messagingSenderId: "235397869371",
      appId: "1:235397869371:web:84ade3b59feceaaaac911e",
      measurementId: "G-DMRRZ8KQ5D"
    };

    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    const database = firebase.database();

    const dataRef = database.ref('order_all');

    dataRef.on('value', (snapshot) => {
      const fetchedData = [];
      snapshot.forEach((childSnapshot) => {
        const key = childSnapshot.key;
        const childData = childSnapshot.val();

        fetchedData.push({
          key: key,
          data: childData
        });
      });
      console.log("fetchedData", fetchedData)
    });
  }, []);


  const { children } = props;
  return accessGranted ? <React.Fragment>{!isLoading && children}</React.Fragment> : null;
}

export default Auth;
