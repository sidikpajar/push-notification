/* eslint-disable no-undef */
import axios from "axios";

const api = axios.create({
  baseURL: process.env.API_URL,
  headers: { "Content-Type": "application/json" },
  /* other custom settings */
  validateStatus: () => true,
});

export default api;
